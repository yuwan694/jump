#!/bin/bash

##
## by yuwan@innopiatech.com
##
##	[2005-04-01 YUWAN] v1.00 released
##

##
## To Do
##	* (none)
##


##
##	Global Definitions
##
declare PROG_NAME="J.SH"		## program name
declare PROG_VER="1.00"			## program version
declare JUMPRC="${HOME}/.jumprc"	## program resource file


##------------------------------------------------------------------------
##	Recent visited directories, name r utility
##------------------------------------------------------------------------

##
##	1. Use command "r [idx]"
##		$ r	: list recent directories
##		$ r 2	: jump to recent_dir index 2
##		$ r2	: same as "r 2"
##		$ rc	: delete all recent dir list
##		$ rc 2	: delete recent dir index 2
##

##
##	Config / Initial Values
##
declare -a RCNT_DIRS		## array of recent dirs
declare MAX_RCNT_DIRS=20	## number of recent dirs
NEXT_RCNT_INDEX=0		## next entry to overwrite

##
##	Put cur_dir to rcnt_dirs
##
function put_cur_dir_to_rcnts()
{
	## get arguments
	declare CUR_DIR="$1"

	## append new recent directory
	RCNT_DIRS[$NEXT_RCNT_INDEX]="${CUR_DIR}"

	NEXT_RCNT_INDEX=`expr ${NEXT_RCNT_INDEX} + 1`
	if [ "${NEXT_RCNT_INDEX}" -ge "${MAX_RCNT_DIRS}" ]; then
		NEXT_RCNT_INDEX=0
	fi
}

##
##	r utility
##
function r()
{
	## r
	## : list rcnt_entries
	if [ "$#" -lt "1" ]; then
		declare INDEX=0
		echo ""
		while [ "${INDEX}" -lt "${MAX_RCNT_DIRS}" ]; do
			if [ "${INDEX}" = "${NEXT_RCNT_INDEX}" ]; then
				echo "- - - - - - - - - - - - - - - -"
			fi

			if [ "x${RCNT_DIRS[$INDEX]}" != "x" ]; then
				echo "${INDEX}: ${RCNT_DIRS[$INDEX]}"
			fi

			INDEX=`expr ${INDEX} + 1`
		done
		echo ""
		return
	fi

	## r [idx]
	## : jump to Nth entry
	declare INDEX="$1"
	if [ "x${RCNT_DIRS[$INDEX]}" = "x" ]; then
		echo "${PROG_NAME}: r: invalid index ${INDEX}"
		return
	else
		builtin cd "${RCNT_DIRS[$INDEX]}"
	fi
}

##
##	rc [idx]
##	: clear recent dir list
##
function rc()
{
	## delete indexed only
	if [ "$#" -ge "1" ]; then
		## delete index
		declare INDEX="$1"
		RCNT_DIRS[$INDEX]=""
		return
	fi

	## confirm for delete all
	declare USER_SAYS
	echo -n "${PROG_NAME}: Delete all recent dir list? (y/n) "
	read USER_SAYS
	if [ "${USER_SAYS}" != "y" ]; then
		return
	fi

	## delete all recent dirs
	declare INDEX=0
	while [ "${INDEX}" -lt "${MAX_RCNT_DIRS}" ]; do
		RCNT_DIRS[$INDEX]=""
		INDEX=`expr ${INDEX} + 1`
	done
}

##------------------------------------------------------------------------
##	Enhanced j utility, name k utility
##------------------------------------------------------------------------

##
##	"Enhanced Jump", stores Leaf Directories visited.
##	This utility rewrite builtin cd function.
##
##	1. Source this script to load functions to current shell
##		$ . ./j.sh
##	   or
##		$ source ./j.sh
##
##	2. This utility replace builtin cd() function.
##	   The usage is same with original cd function,
##	   but it has additional operation to save leaf dirs to env.
##		$ cd .		: add cur_dir to leafs
##		$ cd dir	: cd to "dir", and add cur_dir to leafs
##
##	3. Use commands "k [idx [dir | num]]"
##		$ k		: list leaf directories
##		$ k 2		: jump to leaf dir index 2
##		$ k2		: short format without space, equal to "k 2"
##		$ k 2 2		: jump to leaf dir index 2 and go up 2 times
##		$ k 2 ../..	: equivalant to "k 2 2"
##		$ k 2 ../oth_dir
##				: jump to ../oth_dir,
##				: which is relative to leaf dir of index 2
##		$ kc		: clear all leaf dirs
##		$ kc 2		: delete leaf dir with index 2
##
##	4. k utility do not support following command,
##	   because these commands are not proper to k utility design.
##		$ kb		: backup current leaf_dirs
##		$ kr		: restore current leaf_dirs
##

##
##	Config / Initial Values
##
declare -a LEAF_DIRS		## array of leaf dirs
declare MAX_LEAF_DIRS=20	## number of leaf dirs
NEXT_LEAF_INDEX=0		## next entry to overwrite

##
##	Try replace leaf_dir entry with cur_dir
##
##	Returns:
##		0: leaf_dir is not matching with cur_dir
##		1: leaf_dir replaced with cur_dir
##		2: leaf_dir is matching with cur_dir, and same or longer
##
function replace_leaf_dir_entry()
{
	## get arguments
	declare LEAF_DIR="$1/"
	declare CUR_DIR="$2/"
	declare INDEX="$3"

	declare LEAF_DIR_LEN=${#LEAF_DIR}
	declare CUR_DIR_LEN=${#CUR_DIR}

	if [ "${LEAF_DIR_LEN}" -gt "${CUR_DIR_LEN}" ]; then
		LEAF_DIR_PREFIX="${LEAF_DIR:0:${CUR_DIR_LEN}}"
		if [ "${LEAF_DIR_PREFIX}" = "${CUR_DIR}" ]; then
			return 2	## Leaf is matching & longer
		else
			return 0	## not matching
		fi
	elif [ "${LEAF_DIR_LEN}" -lt "${CUR_DIR_LEN}" ]; then
		CUR_DIR_PREFIX="${CUR_DIR:0:${LEAF_DIR_LEN}}"
		if [ "${CUR_DIR_PREFIX}" = "${LEAF_DIR}" ]; then
			LEAF_DIRS[$INDEX]="$2"
			return 1	## Leaf replaced with Cur
		else
			return 0	## not matching
		fi
	else
		if [ "${LEAF_DIR}" = "${CUR_DIR}" ]; then
			return 2	## same
		else
			return 0	## not matching
		fi
	fi
}

##
##	Try put cur_dir to leaf_dirs
##		1. try to find matching leaf_dir, or replace a leaf_dir
##		2. add cur_dir to empty leaf_dir entry
##		3. overwrite existing leaf_dir with cur_dir
##
function put_cur_dir_to_leafs()
{
	## get arguments
	declare CUR_DIR="$1"

	## search existing leaf to replace
	declare INDEX=0
	while [ "${INDEX}" -lt "${MAX_LEAF_DIRS}" ]; do
		if [ "x${LEAF_DIRS[$INDEX]}" = "x" ]; then
			INDEX=`expr ${INDEX} + 1`
			continue
		fi

		replace_leaf_dir_entry "${LEAF_DIRS[$INDEX]}" "${CUR_DIR}" "${INDEX}"

		if [ "$?" != "0" ]; then
			return		## matching found or replaced
		fi

		INDEX=`expr ${INDEX} + 1`
	done

	## insert new leaf at empty entry
	declare INDEX=0
	while [ "${INDEX}" -lt "${MAX_LEAF_DIRS}" ]; do
		if [ "x${LEAF_DIRS[$INDEX]}" = "x" ]; then
			LEAF_DIRS[$INDEX]="${CUR_DIR}"
			return		## add new leaf_dir
		fi

		INDEX=`expr ${INDEX} + 1`
	done

	## overwrite existing one
	LEAF_DIRS[$NEXT_LEAF_INDEX]="${CUR_DIR}"

	NEXT_LEAF_INDEX=`expr ${NEXT_LEAF_INDEX} + 1`
	if [ "${NEXT_LEAF_INDEX}" -ge "${MAX_LEAF_DIRS}" ]; then
		NEXT_LEAF_INDEX=0
	fi
}

##
##	k utility
##
function k()
{
	## k
	## : list leaf_entries
	if [ "$#" -lt "1" ]; then
		declare INDEX=0
		echo ""
		while [ "${INDEX}" -lt "${MAX_LEAF_DIRS}" ]; do
			if [ "x${LEAF_DIRS[$INDEX]}" != "x" ]; then
				echo "${INDEX}: ${LEAF_DIRS[$INDEX]}"
			fi

			INDEX=`expr ${INDEX} + 1`
		done
		echo ""
		return
	fi

	## k [idx]
	## : jump to Nth entry
	declare INDEX="$1"
	if [ "x${LEAF_DIRS[$INDEX]}" = "x" ]; then
		echo "${PROG_NAME}: k: invalid index ${INDEX}"
		return
	else
		builtin cd "${LEAF_DIRS[$INDEX]}"
	fi

	if [ "$#" -lt "2" ]; then
		return	## no relative cd
	fi

	## k idx [num | dir]
	## : cd to directory which is relative to leaf_dir
	declare UP_LEVEL="$2"
	expr ${UP_LEVEL} + 1 > /dev/null 2>&1
	declare IS_NUMERIC=$?
	if [ "${IS_NUMERIC}" = "0" ]; then
		## k idx num
		## : cd to relative up directory num times
		while [ "${UP_LEVEL}" -gt "0" ]; do
			builtin cd ..
			UP_LEVEL=`expr ${UP_LEVEL} - 1`
		done
	else
		## k idx dir
		## : cd to relative to leaf_dir
		builtin cd "${UP_LEVEL}"

		## put cur dir to leafs array
		put_cur_dir_to_leafs "${PWD}"
	fi
}

##
##	kc [idx]
##	: clear leaf dir list
##
function kc()
{
	## delete indexed only
	if [ "$#" -ge "1" ]; then
		## delete index
		declare INDEX="$1"
		LEAF_DIRS[$INDEX]=""
		return
	fi

	## confirm for delete all
	declare USER_SAYS
	echo -n "${PROG_NAME}: Delete all leaf dir list? (y/n) "
	read USER_SAYS
	if [ "${USER_SAYS}" != "y" ]; then
		return
	fi

	## delete all leaf dirs
	declare INDEX=0
	while [ "${INDEX}" -lt "${MAX_LEAF_DIRS}" ]; do
		LEAF_DIRS[$INDEX]=""
		INDEX=`expr ${INDEX} + 1`
	done
}

##
##	Add cur_dir to arrays
##
function add_cur_dir()
{
	## put cur dir to leafs array
	put_cur_dir_to_leafs "${PWD}"

	## put cur dir to recent array
	put_cur_dir_to_rcnts "${PWD}"
}

##
##	Re-written cd function
##
function cd()
{
	## save prev-dir
	declare CUR_DIR="${PWD}"

	## cd -
	## : jump to $OLDPWD
	if [ "x$1" = "x-" ]; then
		builtin cd "${OLDPWD}"
		return
	fi

	## execute original cd cmd
	if [ "$#" -lt "1" ]; then
		builtin cd
	else
		builtin cd "$*"
	fi

	## no change directory
	if [ "${CUR_DIR}" = "${PWD}" ]; then
		return
	fi

	## add new dir
	add_cur_dir
}


##------------------------------------------------------------------------
##	Following is the original j utility
##------------------------------------------------------------------------

##
##	"Jump", Directory Jumping Utility
##
##	1. Source this script to load functions to current shell
##		$ . ./j.sh
##	   or
##		$ source ./j.sh
##
##	2. Use commands (j jc jl js jb jr),
##		$ jc	: clear all entry
##		$ jc 2	: clear entry index 2
##		$ jl	: list all entry
##		$ j	: list all entry, same as jl
##		$ j 3	: jump to entry 3
##		$ j3	: jump to entry 3, same as j 3
##		$ js	: set entry with current directory, use empty one.
##		$ js 4	: set entry 4 with current directory
##		$ jb	: backup current directory list to ~/.jumprc
##		$ jr	: restore current directory list to ~/.jumprc
##

##
##	Configuration
##
declare -a JUMP_DIRS		## array of jump dirs
declare -a DIR			## backward compatible, old jump dirs
declare MAX_JUMP_INDEX=20	## number of directory entries


##
##	Jump Functions
##

##	Clear Jump Dirs		##############
function jc()
{
	declare INDEX="$1"

	## index not specified
	if [ "x${INDEX}" == "x" ]; then

		## clear all entries
		INDEX=0
		while [ "${INDEX}" -lt "${MAX_JUMP_INDEX}" ]; do
			JUMP_DIRS[$INDEX]=""
			INDEX=`expr ${INDEX} + 1`
		done

		return
	fi

	## index is not valid
	if [ "${INDEX}" -lt "0" -o "${INDEX}" -gt "${MAX_JUMP_INDEX}" ]; then
		echo "${PROG_NAME}: ERR: ${INDEX} is not valid index."
		return
	fi

	## clear specified index
	JUMP_DIRS[$INDEX]=""
}

##	List Jump Dirs		##############
function jl()
{
	declare INDEX=0
	echo ""
	while [ "${INDEX}" -lt "${MAX_JUMP_INDEX}" ]; do
		if [ "x${JUMP_DIRS[$INDEX]}" != "x" ]; then
			echo "${INDEX}: ${JUMP_DIRS[$INDEX]}"
		fi
		INDEX=`expr ${INDEX} + 1`
	done
	echo ""
}

##	Backup Jump Dirs	##############
function jb()
{
	echo "#!/bin/bash" > ${JUMPRC}
	echo "## Jump Directories" >> ${JUMPRC}

	declare INDEX=0
	while [ "${INDEX}" -lt "${MAX_JUMP_INDEX}" ]; do
		if [ "x${JUMP_DIRS[$INDEX]}" != "x" ]; then
			echo "JUMP_DIRS[${INDEX}]=${JUMP_DIRS[$INDEX]}" >> ${JUMPRC}
		fi
		INDEX=`expr ${INDEX} + 1`
	done
}

##	Restore Jump Dirs	##############
function jr()
{
	source ${JUMPRC}

	## for backward compatibility
	declare INDEX=0
	while [ "${INDEX}" -lt "${MAX_JUMP_INDEX}" ]; do
		if [ "x${DIR[$INDEX]}" != "x" ]; then
			JUMP_DIRS[$INDEX]="${DIR[$INDEX]}"
			unset DIR[$INDEX]
		fi
		INDEX=`expr ${INDEX} + 1`
	done
}

##	Jump to ..		##############
function j()
{
	declare INDEX="$1"

	## index not specified
	if [ "x${INDEX}" == "x" ]; then
		jl	## show list
		return
	fi

	## index is not valid
	if [ "${INDEX}" -lt "0" -o "${INDEX}" -gt "${MAX_JUMP_INDEX}" ]; then
		echo "${PROG_NAME}: ERR: ${INDEX} is not valid index."
		return
	fi

	## empty directory
	if [ "x${JUMP_DIRS[${INDEX}]}" == "x" ]; then
		echo "${PROG_NAME}: Warn: target is (empty) directory."
		return
	fi

	## action!
	cd "${JUMP_DIRS[${INDEX}]}"
}

##	Short Form		##############
function define_short_forms()
{
	## j[idx]
	declare INDEX=0
	while [ "${INDEX}" -lt "${MAX_JUMP_INDEX}" ]; do
		alias j${INDEX}="j ${INDEX}"
		INDEX=`expr ${INDEX} + 1`
	done

	## k[idx]
	declare INDEX=0
	while [ "${INDEX}" -lt "${MAX_LEAF_DIRS}" ]; do
		alias k${INDEX}="k ${INDEX}"
		INDEX=`expr ${INDEX} + 1`
	done

	## r[idx]
	declare INDEX=0
	while [ "${INDEX}" -lt "${MAX_RCNT_DIRS}" ]; do
		alias r${INDEX}="r ${INDEX}"
		INDEX=`expr ${INDEX} + 1`
	done
}

## Set Jump Directory		##############
function js()
{
	declare HERE="`pwd`"

	## Search for the same entry
	declare INDEX=0
	while [ "${INDEX}" -lt "${MAX_JUMP_INDEX}" ]; do
		if [ "${JUMP_DIRS[$INDEX]}" == "${HERE}" ]; then
			echo "${PROG_NAME}: Info: same entry found at ${INDEX}."
			return
		fi
		INDEX=`expr ${INDEX} + 1`
	done

	## get index
	declare INDEX="$1"

	## index not specified
	if [ "x${INDEX}" == "x" ]; then

		## find first empty entry
		INDEX=0
		while [ "${INDEX}" -lt "${MAX_JUMP_INDEX}" ]; do
			if [ "x${JUMP_DIRS[$INDEX]}" == "x" ]; then
				break	## found
			fi
			INDEX=`expr ${INDEX} + 1`
		done

		## not found
		if [ "${INDEX}" -ge "${MAX_JUMP_INDEX}" ]; then
			echo "${PROG_NAME}: ERR: no empty entry, specify entry."
			return
		fi

		echo "${PROG_NAME}: Info: use first empty index ${INDEX}."
	fi

	## index is not valid
	if [ "${INDEX}" -lt "0" -o "${INDEX}" -gt "${MAX_JUMP_INDEX}" ]; then
		echo "${PROG_NAME}: ERR: ${INDEX} is not valid index."
		return
	fi

	## action!
	JUMP_DIRS[${INDEX}]="${HERE}"
}


##
##	Main Helper Functions
##

function check_jump_utility()
{
	declare UTILITY="$1"
	type ${UTILITY} > /dev/null 2>&1
	if [ "$?" != "0" ]; then
		echo "${PROG_NAME}: ERR: utility ${UTILITY} not found."
		return
	fi
}

function check_jump_utils()
{
	check_jump_utility expr
}

##
##	Cowork with abc utility
##
function acd()
{
	## load abc config
	source ~/.abcrc

	## jump to current abc directory
	if [ "x${ABC_CUR_DIR}" != "x" ]; then
		cd "${ABC_CUR_DIR}"
	fi
}

##
##	Main Function
##
check_jump_utils		## check utilities required
define_short_forms		## define short form commands as aliases
add_cur_dir			## store current directory to arrays
