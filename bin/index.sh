#!/bin/bash

my_index_html="000_index.html"
my_index_anchor="000_index.anchor"
my_anchored_html="000_anchored.html"
my_anchor_interval="10"
my_anchor_base="100"
my_divided_basename="000_divided"
my_divide_interval="50"
my_divide_base="100"

## This tool generates ${my_index_html} in the current directory,
## which includes all the <img src=... width=100%> tags,
## for all files found.
## It is useful, if you would like to see all the pics
## in a web browser page, by scrolling.

find . -type f | sort -n > ${my_index_html}
sed -i 's/^/<img src="/g' ${my_index_html}
sed -i 's/$/" width=100%><br>/g' ${my_index_html}

function anchor()
{
  my_anchor_index=${my_anchor_base}
  my_count=0
  while read -r line
  do
    echo "${line}"
    ((my_count = my_count + 1 ))
    if [ ${my_count} -ge ${my_anchor_interval} ]
    then
      my_count=0
      ((my_anchor_index = my_anchor_index + 1))
      my_anchor="anchor-${my_anchor_index}"
      echo "<h3><a name=\"${my_anchor}\"></a><a href=\"#TOC\">TOC</a></h3>"
    fi
  done < ${my_index_html} > ${my_index_anchor}
}

function toc() {
  (
    echo "<a name=\"TOC\"></a>"
    echo "<ul>"
    my_anchor_max=$(grep 'anchor-' ${my_index_anchor} | grep -c 'TOC')
    ((my_anchor_max = my_anchor_max + my_anchor_base))
    for my_anchor_index in $(seq ${my_anchor_base} ${my_anchor_max})
    do
      my_anchor="anchor-${my_anchor_index}"
      echo "<li><a href=\"#${my_anchor}\">${my_anchor}</a></li>"
    done
    echo "</ul>"
    grep -v ${my_index_html} ${my_index_anchor} | grep -v "${my_anchored_html}"
  ) > ${my_anchored_html}
}

function remove_lines_with_pattern()
{
  my_file="$1"
  my_patterns="${my_index_html} ${my_index_anchor} ${my_anchored_html} ${my_divided_basename} Thumbs.db"
  for my_pattern in ${my_patterns}
  do
    sed -i "/${my_pattern}/d" "${my_file}"
  done
}

function divide()
{
  my_divide_index=${my_divide_base}
  my_divided_html="${my_divided_basename}.${my_divide_index}.html"
  my_count=0
  while read -r line
  do
    echo "${line}" >> ${my_divided_html}
    ((my_count = my_count + 1 ))
    if [ ${my_count} -ge ${my_divide_interval} ]
    then
      my_count=0
      ((my_divide_index = my_divide_index + 1))
      my_divided_html="${my_divided_basename}.${my_divide_index}.html"
    fi
  done < ${my_index_html}
}

function main() {
  remove_lines_with_pattern ${my_index_html}

  anchor
  toc
  remove_lines_with_pattern ${my_anchored_html}
  rm -f ${my_index_anchor}

  rm -f ${my_divided_basename}*.html
  divide
}

main
