# j.sh or jump.sh

Shell functions help you jumping around directories.


## Introduction
When I wrote the first jump.sh, around 1995, it was on top of Bourne Shell.
And at that time the shell itself wasn't that great,
so I have to call the script explicitly, no function or no aliases.
Because of that, and also because of my limited knowledge in shell scripting,
the jump.sh was not that useful.
But later, I still felt I need that kind of tool anyway,
so retrieved old code, and start improving, with new name j.sh.
The Bash (Bourne-Again shell) has enough features I need, so this time I could match my needs.
The tool was very useful, and I've heavily used,
especially when I browsing embedded linux kernel source codes.

## Usage
1) load j.sh in the current running shell.
```
$ source j.sh
```
   
2) Try r command, which shows recent directories visited.
```
$ r  # without argument it will list directories

0: /data/wyu/tmp/abc
1: /data/wyu
2: /data/wyu/work
3: /data/wyu/work/jump
- - - - - - - - - - - - - - - -

$ r0  # move to the 0 directory above, which is /data/wyu/tmp/abc
```
   
3) Try k command, which keeps and shows only longest path visited.
```
$ k  # show longest paths visited

0: /data/wyu/tmp/abc
1: /data/wyu/work/jump

$ k0  # move to /data/wyu/tmp/abc
$ k0 1  # move to /data/wyu/tmp
$ k0 2  # move to /data/wyu
```
    
4) Try j command, you can register directories.
```
$ js  # register current directory

$ j  # show registerred directories

0: /data/wyu/tmp
1: /data/wyu/work

$ j0  # move to /data/wyu/tmp
$ j1  # move to /data/wyu/work
```
    
5) Add j.sh loading in to the user login shell profile, like ~/.profile or ~/.bash_profile.
```
$ cp j.sh ~
$ echo "source ~/j.sh" >> ~/.profile
```


## Help from original source

```
##------------------------------------------------------------------------
##  Recent visited directories, name r utility
##------------------------------------------------------------------------

##
##  1. Use command "r [idx]"
##      $ r : list recent directories
##      $ r 2   : jump to recent_dir index 2
##      $ r2    : same as "r 2"
##      $ rc    : delete all recent dir list
##      $ rc 2  : delete recent dir index 2
##

...

##------------------------------------------------------------------------
##  Enhanced j utility, name k utility
##------------------------------------------------------------------------

##
##  "Enhanced Jump", stores Leaf Directories visited.
##  This utility rewrite builtin cd function.
##
##  1. Source this script to load functions to current shell
##      $ . ./j.sh
##     or
##      $ source ./j.sh
##
##  2. This utility replace builtin cd() function.
##     The usage is same with original cd function,
##     but it has additional operation to save leaf dirs to env.
##      $ cd .      : add cur_dir to leafs
##      $ cd dir    : cd to "dir", and add cur_dir to leafs
##
##  3. Use commands "k [idx [dir | num]]"
##      $ k     : list leaf directories
##      $ k 2       : jump to leaf dir index 2
##      $ k2        : short format without space, equal to "k 2"
##      $ k 2 2     : jump to leaf dir index 2 and go up 2 times
##      $ k 2 ../.. : equivalant to "k 2 2"
##      $ k 2 ../oth_dir
##              : jump to ../oth_dir,
##              : which is relative to leaf dir of index 2
##      $ kc        : clear all leaf dirs
##      $ kc 2      : delete leaf dir with index 2
##
##  4. k utility do not support following command,
##     because these commands are not proper to k utility design.
##      $ kb        : backup current leaf_dirs
##      $ kr        : restore current leaf_dirs
##

...

##------------------------------------------------------------------------
##  Following is the original j utility
##------------------------------------------------------------------------

##
##  "Jump", Directory Jumping Utility
##
##  1. Source this script to load functions to current shell
##      $ . ./j.sh
##     or
##      $ source ./j.sh
##
##  2. Use commands (j jc jl js jb jr),
##      $ jc    : clear all entry
##      $ jc 2  : clear entry index 2
##      $ jl    : list all entry
##      $ j : list all entry, same as jl
##      $ j 3   : jump to entry 3
##      $ j3    : jump to entry 3, same as j 3
##      $ js    : set entry with current directory, use empty one.
##      $ js 4  : set entry 4 with current directory
##      $ jb    : backup current directory list to ~/.jumprc
##      $ jr    : restore current directory list to ~/.jumprc
##
```
