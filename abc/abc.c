/**	@file	abc.c
 *	@brief	ABC main implementations
 *
 *	@author	yuwan@innopiatech.com
 *	@date	2005. 10. 18.
 */

#define _ABC_C

#include "abc.h"

/* outside functions */
extern char* strcpy();
extern char* get_tail();
extern DIR* opendir();
extern struct dirent* readdir();
extern char* getcwd();
extern char* ctime();
extern char* name_of_user();
extern char *cuserid(char *string);

/* User data structure for storing user info */
#define MAXLOGIN 16  /* max number of char in userid that will be printed */
#define UDQ     50  /* udata and devl granularity forstructure allocation */
struct udata {
        long uid;     /* nummeric user id                           */
        char name[MAXLOGIN];    /* character user id, max length of user name */
};
struct udata *ud;               /* pointer to udata         */
int     nud     = 0;            /* number of vaild ud structures */
int     maxud   = 0;            /* number of ud's allocated */
char    itoa_str[16];

CELL *Cellnow = NULL;
PARCEL *Parcelnow = NULL;
MUD *Mudnow = NULL, *Muds[MUD_MAX_CNT];

/* line control */
int	Sep_line;

/* mud control */
int	Mud_num;

/* start up directory */
char Startupdir[DIRLEN] = {'\0', };
char Hostname[32];			/* hostname */
char Username[32];

char *trimwhitespace(char *str)
{
    char *end;

    // Trim leading space
    while(isspace((unsigned char)*str)) str++;

    if(*str == 0)  // All spaces?
        return str;

    // Trim trailing space
    end = str + strlen(str) - 1;
    while(end > str && isspace((unsigned char)*end)) end--;

    // Write new null terminator character
    end[1] = '\0';

    return str;
}

char *ltrim(char *str)
{
    // Trim leading space
    while(isspace((unsigned char)*str)) str++;
    return str;
}

char *rtrim(char *str)
{
    char *end;

    if(*str == 0)  // All spaces?
        return str;

    // Trim trailing space
    end = str + strlen(str) - 1;
    while(end > str && isspace((unsigned char)*end)) end--;

    // Write new null terminator character
    end[1] = '\0';

    return str;
}

/***************************************************************************
**	Save current dir to ABCRC #save_cur_dir
***************************************************************************/
void save_cur_dir(void)
{
	static char szCurDir[DIRLEN] = {'\0',};

	if ( strcmp(szCurDir, CURRENT_DIR) != 0 ) {
		char szCmd[DIRLEN];

		strcpy(szCurDir, CURRENT_DIR);
		sprintf(szCmd, "echo \"export ABC_CUR_DIR=%s\" > %s/%s",
			CURRENT_DIR, getenv("HOME"), ABCRC);
		system(szCmd);
	}
}
/***************************************************************************
**	Adjust directory names	#adjust_dir
***************************************************************************/
#ifdef	USAGE
		switch( adjust_dir(abs, rel, save) ) {
		    case 1:	/* abs not abs_form */
			break;
		    case 2:	/* can't open save */
			break;
		    case 3:	/* save is not a dir */
			break;
		    case 0:	/* success */
			break;
		}
#endif
int adjust_dir(char* abs, char* rel, char* save)
{
	char absolute[DIRLEN], relative[DIRLEN];
	struct stat tmp;
	ushort modetmp;
	ushort modedir;
	char token[60];
	char *absgo, *relgo, *tokengo;

	strcpy(absolute, abs);	strcpy(relative, rel);
	if ( absolute[0] != '/' ) {
		return(1);	/* error 1 : absolute is not a abs_form */
	}

	if ( relative[0] == '/' ) {	/* relative is abs_form */
		strcpy(absolute, relative);
	} else {			/* adjust absolute with relative */
		relgo = relative;
		if ( strcmp(absolute, "/") != 0 ) {
			absgo = absolute;
			while( *absgo != '\0' ) absgo++;
		} else {
			absgo = absolute;
		}
		while ( *relgo != '\0' ) {
			int token_found = 0;
			token[0] = '\0';
			tokengo = token;
			while ( !token_found ) {
				switch( *relgo ) {
				    case '/':
					token_found = 1;
					relgo++;
					break;
				    case '\0':
					token_found = 1;
					break;
				    default:
					*tokengo = *relgo;
					tokengo++; relgo++;
					break;
				}
			}
			*tokengo = '\0';

			if ( !strcmp(token, ".") ) {
				/* doing nothing */
			} else if ( !strcmp(token, "..") ) {
				if ( absgo != absolute ) {
					absgo--;
					while( *absgo != '/' ) absgo--;
				}
			} else {
				*absgo = '/';
				absgo++;
				strcpy(absgo, token);
				while ( *absgo != '\0' ) absgo++;
			}
		}
		if ( absgo == absolute ) {
			strcpy(absolute, "/");
		} else {
			*absgo = '\0';
		}
	}

	/* check for new directory */
	if ( stat(absolute, &tmp) == -1 ) {
		strcpy(save, absolute);
		return(2);	/* error 2 : cannot get status of new_dir */
	}
	modetmp = tmp.st_mode;
	modedir = modetmp & 0070000u;
	if ( modedir != 0040000u ) {
		strcpy(save, absolute);
		return(3);	/* error 3 : it's not directory */
	}

	strcpy(save, absolute);
	return(0);		/* success */
}

/***************************************************************************
**	Command station		#com_station
***************************************************************************/
void com_station()
{
	int pre_num = 0;
	int ret;
	while ( 1 ) {
		save_cur_dir();

		switch( ret = getchar() ) {
		/* mud change command */
		    case 'm': mud_change(); break;
		/* mode change command */
		    case '~': mud_mode_change(); break;
		    case ',': parcel_mode_change(); break;
		    case '|': toggle_dmode(); break;
		/* moving cursor command */
		    case 'j': move_down(); break;
		    case 'k': move_up(); break;
		    case 'h': move_left(); break;
		    case 'l': move_right(0); break;
		    case CRTL_F: move_down_page(); break;
		    case CRTL_B: move_up_page(); break;
		    case CRTL_E: move_down_line(); break;
		    case CRTL_Y: move_up_line(); break;
		    case 'H': move_head('H'); break;
		    case 'L': move_head('L'); break;
		    case 'M': move_head('M'); break;
		    case 'G': move_position(pre_num); break;
		/* give information command */
		    case CRTL_G: file_num(); break;
		/* draw command */
		    case CRTL_L:
		    case 'r': clear(); redraw(); rrr(); break;
		/* re-read command */
		    case 'R': re_read(CURRENT_DIR, 1); break;
		    case 'T': sort_method(); break;
		/* change directory command */
		    case '\n': case '\r':
			{
				unsigned int nStat;
				nStat = Cellnow->list.st_mode & S_IFMT;

				if ( nStat==S_IFDIR || nStat==S_IFLNK )
					change_dir();
				else if ( nStat==S_IFIFO || nStat==S_IFSOCK )
					break;
				else if ( nStat==S_IFREG )
					edit_file('e');
			}
			break;
		    case 'c': change_dir(); break;
		    case 'C': cd_argu(); break;
		/* edit commands */
		    case 'e': edit_file('e'); break;
		    case 's': edit_file('s'); break;
		/* search pattern command */
		    case '/': search_fname(1); break;
		    case 'n': search_fname(0); break;
		/* select command */
		    case 'i': select_cell(); break;
		    case 'u': unselect(); break;
		    case '+': select_all(); break;
		    case '-': unselect_all(); break;
		    case 'I': tell_select(); break;
		    case '?': help_me(); break;
		/* compare directory */
		    case '%': comp_dir(); break;
		/* compare files */
		    case '=': over_comp(getchar()); break;
		/* direction key */
		    case '':
			if ( getchar() == '[' ) {
				switch( getchar() ) {
				    case 'A': move_up(); break;
				    case 'B': move_down(); break;
				    case 'C': move_right(0); break;
				    case 'D': move_left(); break;
				}
			}
			break;
		/* dangerous commands */
		    case 'p':
			if ( pre_num != 26 ) break;
			printerr("!!! Caution !!! <<< DANGEROUS COMMANDS MODE >>> !!! Caution !!!");
			rrr();
			switch( getchar() ) {
			/* deleting commands */
			    case 'd': delete_files('d'); break;
			    case 'D': delete_files('D'); break;
			/* copy commands */
			    case 'c': copy_files(); break;
			    case 'C': over_send('C'); break;
			/* move commands */
			    case 'm': move_files(); break;
			    case 'M': over_send('M'); break;
			/* copy directory with sub_directories */
			    case '^': cp_dir(); break;
			    default:
				printerr(""); rrr();
			}
			break;
		    case '!': shell_cmd(); break;
		/* exit command station */
		    case 'q':
			printerr("Press \"Q\" right now !!!"); rrr();
			switch ( getchar() ) {
			    case 'Q':
				return;
			    default:
				printerr("Continue ABC");
				rrr(); break;
			}
			break;
		/* pre_number control */
		    case '$': while ( move_right(0) == 0 ) ; break;
		    case '0':
			if ( pre_num == 0 )
				while ( move_left() == 0 ) ;
			//!< don't break;
		    case '1': case '2': case '3': case '4':
		    case '5': case '6': case '7': case '8':
		    case '9': pre_num = pre_num * 10 + ( ret - '0' ); break;
		}
		if ( ret < '0' || ret > '9' ) pre_num = 0;
	}
}
/****************************************************************************
**	Mode Change	#mode_change
****************************************************************************/
void mud_mode_change()
{
	switch( SELECT_PARCEL ) {
	    case 's':	/* single is selected */
		Parcelnow = SPARCEL;
		switch( PARCEL_MODE ) {
		    case 's':	/* single - normal */
			PARCEL_MODE = 'u'; break;
		    case 'S':	/* single - long */
			PARCEL_MODE = 'U'; break;
		}
		parcel_line();
		DUPARCEL = Parcelnow;
		if ( DLPARCEL == (PARCEL *)NULL ) {
			Parcelnow = DLPARCEL;
			switch( get_newparcel(Startupdir, 'n') ) {
			    case 1:	/* cannot get memory for Parcelnow */
				printerr2("Cannot get memory for Parcelnow");
				exit(1);
			    case 2:	/* cannot open directory */
				printerr2("Cannot open directory");
				free(Parcelnow);
				exit(1);
			    case 3:	/* cannot get memory for CELLs */
				printerr2("Cannot get memory for cells");
				free(Parcelnow);
				exit(1);
			    case 0:	/* success */
				/* decide parcel_mode
				 * & dir_line,mini_line,upmost_line,downlast
				 */
				DLPARCEL = Parcelnow;
				PARCEL_MODE = 'l';
				parcel_line();
				break;
			}
		}
		SELECT_PARCEL = 'u';
		redraw();
		break;
	    case 'u':	/* double up */
		Parcelnow = DUPARCEL;
		switch( PARCEL_MODE ) {
		    case 'u':	/* double - up - normal */
			PARCEL_MODE = 's'; break;
		    case 'U':	/* double - up - long */
			PARCEL_MODE = 'S'; break;
		}
		parcel_line();
		SPARCEL = Parcelnow;
		SELECT_PARCEL = 's';
		redraw();
		break;
	    case 'l':	/* double down */
		Parcelnow = DLPARCEL;
		switch( PARCEL_MODE ) {
		    case 'l':	/* double - low - normal */
			PARCEL_MODE = 's'; break;
		    case 'L':	/* double - low - long */
			PARCEL_MODE = 'S'; break;
		}
		parcel_line();
		SPARCEL = Parcelnow;
		Parcelnow = DUPARCEL;
		switch( PARCEL_MODE ) {
		    case 'u':	/* double - up - normal */
			PARCEL_MODE = 'l'; break;
		    case 'U':	/* double - up - long */
			PARCEL_MODE = 'L'; break;
		}
		parcel_line();
		DLPARCEL = Parcelnow;
		SELECT_PARCEL = 's';
		redraw();
		break;
	}
}

void parcel_mode_change()
{
	int dsp_area;
	dsp_area = DOWNLAST_LINE - UPMOST_LINE + 1;
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
		PARCEL_MODE = 'S';
		while ( POSITION >= DSPHEAD + dsp_area ) DSPHEAD++;
		break;
	    case 'S':	/* single - long */
		PARCEL_MODE = 's'; break;
	    case 'u':	/* double - up - normal */
		PARCEL_MODE = 'U';
		while ( POSITION >= DSPHEAD + dsp_area ) DSPHEAD++;
		break;
	    case 'U':	/* double - up - long */
		PARCEL_MODE = 'u'; break;
	    case 'l':	/* double - low - normal */
		PARCEL_MODE = 'L';
		while ( POSITION >= DSPHEAD + dsp_area ) DSPHEAD++;
		break;
	    case 'L':	/* double - low - long */
		PARCEL_MODE = 'l'; break;
	}
	parcel_line();
	draw_parcel();
}
void toggle_dmode()
{
	switch( SELECT_PARCEL ) {
	    case 'u':	/* double up */
		SELECT_PARCEL = 'l'; redraw(); break;
	    case 'l':	/* double down */
		SELECT_PARCEL = 'u'; redraw(); break;
	}
}
/****************************************************************************
**	move cursor command	#move_cur
****************************************************************************/
void move_down()
{
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
		if ( POSITION + CELL_PER_COL <= TAIL ) {
			Cellnow = POSITION;
			POSITION += CELL_PER_COL; draw_cell(0);
			Cellnow = POSITION; draw_cell(1);
			draw_mini(MINI_LINE);
			rrr();
		} else if ( POSITION != TAIL ) {
			Cellnow = POSITION;
			POSITION = TAIL; draw_cell(0);
			Cellnow = POSITION; draw_cell(1);
			draw_mini(MINI_LINE);
			rrr();
		}
		break;
	    case 'S':	/* single - long */
	    case 'U':	/* double - up - long */
	    case 'L':	/* double - low - long */
		if ( POSITION < TAIL ) {
			Cellnow = POSITION;
			POSITION++; draw_cell(0);
			Cellnow = POSITION; draw_cell(1);
			rrr();
		}
		break;
	}
}
void move_down_page()
{
	int dsp_area;
	dsp_area = DOWNLAST_LINE - UPMOST_LINE + 1;
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
		if ( DSPHEAD + (CELL_PER_COL*dsp_area) <= TAIL ) {
			DSPHEAD += (CELL_PER_COL*dsp_area);
			POSITION = DSPHEAD;
			draw_parcel();
			rrr();
		}
		break;
	    case 'S':	/* single - long */
	    case 'U':	/* double - up - long */
	    case 'L':	/* double - low - long */
		if ( DSPHEAD + dsp_area <= TAIL ) {
			DSPHEAD += dsp_area;
			POSITION = DSPHEAD;
			draw_parcel();
			rrr();
		}
		break;
	}
}
void move_up()
{
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
		if ( POSITION - CELL_PER_COL >= HEAD ) {
			Cellnow = POSITION;
			POSITION -= CELL_PER_COL; draw_cell(0);
			Cellnow = POSITION; draw_cell(2);
			draw_mini(MINI_LINE);
			rrr();
		} else if ( POSITION != HEAD ) {
			Cellnow = POSITION;
			POSITION = HEAD; draw_cell(0);
			Cellnow = POSITION; draw_cell(2);
			draw_mini(MINI_LINE);
			rrr();
		}
		break;
	    case 'S':	/* single - long */
	    case 'U':	/* double - up - long */
	    case 'L':	/* double - low - long */
		if ( POSITION > HEAD ) {
			Cellnow = POSITION;
			POSITION--; draw_cell(0);
			Cellnow = POSITION; draw_cell(2);
			rrr();
		}
		break;
	}
}
void move_up_page()
{
	int dsp_area;
	dsp_area = DOWNLAST_LINE - UPMOST_LINE + 1;
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
		if ( DSPHEAD - CELL_PER_COL*dsp_area >= HEAD ) {
			DSPHEAD -= CELL_PER_COL*dsp_area;
			POSITION = DSPHEAD; draw_parcel();
			rrr();
		} else if ( DSPHEAD != HEAD ) {
			DSPHEAD = HEAD;
			POSITION = DSPHEAD; draw_parcel();
			rrr();
		}
		break;
	    case 'S':	/* single - long */
	    case 'U':	/* double - up - long */
	    case 'L':	/* double - low - long */
		if ( DSPHEAD - dsp_area >= HEAD ) {
			DSPHEAD -= dsp_area;
			POSITION = DSPHEAD; draw_parcel();
			rrr();
		} else if ( DSPHEAD != HEAD ) {
			DSPHEAD = HEAD;
			POSITION = DSPHEAD; draw_parcel();
			rrr();
		}
		break;
	}
}

/**	@brief	Move left a cusor
 *
 *	@return Error Code
 *	@retval	0	successfully moved
 *	@retval 1	Long mode, move left is not supported in this mode
 *	@retval 2	On first column, cannot move left.
 *	@retval 3	This is on the first entry.
 */
int move_left(void)
{
	int nDspIdx, nDspCol;

	if ( PARCEL_MODE >= 'A' && PARCEL_MODE <= 'Z' )
		return 1;	//!< long mode, no move left command

	nDspIdx = Cellnow - DSPHEAD;
	nDspCol = nDspIdx % CELL_PER_COL;
	if ( nDspCol <= 0 )
		return 2;	//!< cannot move left when on first column

	if ( POSITION <= HEAD )
		return 3;	//!< on first entry

	Cellnow = POSITION;
	POSITION--; draw_cell(0);
	Cellnow = POSITION; draw_cell(2);
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
		draw_mini(MINI_LINE);
		break;
	}
	rrr();

	return 0;
}

/**	@brief	Move right a cursor
 *
 *	@param	bForce	Force to move to next element even in the last column
 *
 *	@return	Error Code
 *	@retval	0	successfully moved
 *	@retval 1	Long mode, move right is not supported in this mode
 *	@retval 2	On last column, cannot move right
 *	@retval 3	This is on the last entry
 */
int move_right(int bForce)
{
	int nDspIdx, nDspCol;

	/* if bForce, do not check last column */
	if ( bForce )
		goto force_move_right;

	/* long mode, no move right command */
	if ( PARCEL_MODE >= 'A' && PARCEL_MODE <= 'Z' )
		return 1;

	/* cannot move right on last column */
	nDspIdx = Cellnow - DSPHEAD;
	nDspCol = nDspIdx % CELL_PER_COL;
	if ( nDspCol >= (CELL_PER_COL-1) )
		return 2;

force_move_right:
	/* cannot move right on last entry */
	if ( POSITION >= TAIL )
		return 3;

	/* now move to the next element */
	Cellnow = POSITION;
	POSITION++; draw_cell(0);
	Cellnow = POSITION; draw_cell(1);
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
		draw_mini(MINI_LINE);
		break;
	}
	rrr();

	return 0;
}

int move_down_line(void)
{
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
		if ( TAIL - DSPHEAD >= CELL_PER_COL ) {
			DSPHEAD += CELL_PER_COL;
			while( DSPHEAD > POSITION ) POSITION += CELL_PER_COL;
			draw_parcel();
		}
		break;
	    case 'S':	/* single - long */
	    case 'U':	/* double - up - long */
	    case 'L':	/* double - low - long */
		if ( TAIL > DSPHEAD ) {
			DSPHEAD++;
			while( DSPHEAD > POSITION ) POSITION++;
			draw_parcel();
		}
		break;
	}
}

int move_up_line(void)
{
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
		if ( DSPHEAD - HEAD >= CELL_PER_COL ) {
			int dsp_area;
			DSPHEAD -= CELL_PER_COL;
			dsp_area = DOWNLAST_LINE - UPMOST_LINE + 1;
			while( POSITION - DSPHEAD >= dsp_area*CELL_PER_COL )
				POSITION -= CELL_PER_COL;
			draw_parcel();
		}
		break;
	    case 'S':	/* single - long */
	    case 'U':	/* double - up - long */
	    case 'L':	/* double - low - long */
		if ( DSPHEAD > HEAD ) {
			int dsp_area;
			DSPHEAD--;
			dsp_area = DOWNLAST_LINE - UPMOST_LINE + 1;
			while ( POSITION - DSPHEAD >= dsp_area ) POSITION--;
			draw_parcel();
		}
		break;
	}
}
void move_head(int flag)
	/* H,M,L is available */
{
	int dsp_area;
	dsp_area = DOWNLAST_LINE - UPMOST_LINE + 1;
	Cellnow = POSITION;
	switch( PARCEL_MODE ) {
	    case 's': case 'u': case 'l': /* normal */
		switch( flag ) {
		    case 'H':
                POSITION = DSPHEAD;
                break;
		    case 'M':
                POSITION = DSPHEAD + ( dsp_area * CELL_PER_COL / 2 );
                if ( POSITION > TAIL ) POSITION = TAIL;
                break;
		    case 'L':
                POSITION = DSPHEAD + ( dsp_area * CELL_PER_COL - 1 );
                if ( POSITION > TAIL ) POSITION = TAIL;
                break;
		    default:
		        break;
		}
		break;
	    case 'S': case 'U': case 'L': /* long */
		switch( flag ) {
		    case 'H':
                POSITION = DSPHEAD;
                break;
		    case 'M':
                POSITION = DSPHEAD + ( dsp_area / 2 );
                if ( POSITION > TAIL ) POSITION = TAIL;
                break;
		    case 'L':
                POSITION = DSPHEAD + ( dsp_area - 1 );
                if ( POSITION > TAIL ) POSITION = TAIL;
                break;
		    default:
		        break;
		}
		break;
	}
	draw_cell(0);
	Cellnow = POSITION; draw_cell(1);
	switch( PARCEL_MODE ) {
	    case 's': case 'u': case 'l': /* normal */
		draw_mini(MINI_LINE);
		break;
	    case 'S': case 'U': case 'L': /* long */
		break;
	}
	rrr();
}
void move_position(int num)
{
	Cellnow = POSITION;
	if ( num > 0 ) {
		if ( num <= NUM_OF_CELL ) {
			POSITION = HEAD + ( num - 1 );
		} else {
			printerr("Wrong position."); rrr(); return;
		}
	} else if ( num == 0 ) {
		POSITION = TAIL;
	}
	draw_cell(0);
	Cellnow = POSITION; draw_cell(2);
	switch( PARCEL_MODE ) {
	    case 's': case 'u': case 'l': /* normal */
		draw_mini(MINI_LINE);
		break;
	    case 'S': case 'U': case 'L': /* long */
		break;
	}
	rrr();
}
/***************************************************************************
**	select	#select
***************************************************************************/
void select_cell()
{
	Cellnow = POSITION;
	SELECTED = 'y';
	if ( Cellnow == TAIL ) { draw_cell(0); rrr(); }
	else move_right(FORCE_MOVE);
}
void select_all()
{
	for ( Cellnow = HEAD+2; Cellnow <= TAIL; Cellnow++ ) SELECTED = 'y';
	draw_parcel();
}
void unselect()
{
	Cellnow = POSITION;
	SELECTED = 'n';
	if ( Cellnow == TAIL ) { draw_cell(0); rrr(); }
	else move_right(FORCE_MOVE);
}
void unselect_all()
{
	for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) SELECTED = 'n';
	draw_parcel();
}
void tell_select()
{
	char msg[FILELEN];
	int num = 0;
	for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ )
		if ( SELECTED == 'y' ) num++;
	sprintf(msg, "[ %d ] file(s) selected", num);
	printerr(msg); rrr();
}
/***************************************************************************
**	file_num	#file_num
***************************************************************************/
void file_num()
{
	char msg[64];
	sprintf(msg, "[ %ld ]th file. ( total %d files )"
		, POSITION - HEAD + 1, NUM_OF_CELL);
	printerr(msg); rrr();
}
/***************************************************************************
**	Display commands	#draw_parcel
***************************************************************************/
void redraw()
{
	switch( SELECT_PARCEL ) {
	    case 's':	/* single is selected */
		Parcelnow = SPARCEL;
		draw_parcel();
		break;
	    case 'u':	/* double up */
	    case 'l':	/* double down */
		Parcelnow = DUPARCEL; draw_parcel();
		Parcelnow = DLPARCEL; draw_parcel();
		switch( SELECT_PARCEL ) {
		    case 'u':	/* double up */
			Parcelnow = DUPARCEL; break;
		    case 'l':	/* double down */
			Parcelnow = DLPARCEL; break;
		}
		break;
	}

	/* 2005-12-22
	 *	This is required, otherwise moving cursor not work properly,
	 *	especially when you execute the shell command or edit a file.
	 */
	Cellnow = POSITION;
}
#ifdef USAGE
		1. decide parcel to draw:	Parcelnow = selected_parcel
		2. call draw_parcel();
#endif
void draw_parcel()
{
	int i;
	int dsp_area;

	/* 1 : clear screen area & decide DSPHEAD */
	for ( i=DIR_LINE; i<=DOWNLAST_LINE; i++ ) {
		move(i, 0); clrtoeol();
	}
	dsp_area = DOWNLAST_LINE - UPMOST_LINE + 1;
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
		if ( DSPHEAD > POSITION ) DSPHEAD = POSITION;
		while( POSITION - DSPHEAD >= dsp_area*CELL_PER_COL )
			DSPHEAD += CELL_PER_COL;
		break;
	    case 'S':	/* single - long */
	    case 'U':	/* double - up - long */
	    case 'L':	/* double - low - long */
		if ( DSPHEAD > POSITION ) DSPHEAD = POSITION;
		while ( POSITION - DSPHEAD >= dsp_area ) DSPHEAD++;
		break;
	}

	/* 2 : draw dir_line */
	draw_dir();

	/* 3 : draw Cellnow */
	for ( i=0; DSPHEAD + i <= TAIL; i++ ) {
		Cellnow = DSPHEAD + i;
		if ( draw_cell(0) ) break;
	}

	/* 4 : draw mini_line */
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
		Cellnow = POSITION;
		draw_mini(MINI_LINE);
		break;
	    case 'S':	/* single - long */
	    case 'U':	/* double - up - long */
	    case 'L':	/* double - low - long */
		break;
	}

	/* 5 : clear error_line */
	move(LINES-1, 0); clrtoeol();
	rrr();
}

void draw_dir()
{
	char beautify[DIRLEN];
	char temp[DIRLEN];
	char *go;
	int n, len, i;

	sprintf(temp, "%s:%s: %s", Username, Hostname, CURRENT_DIR);

	if ( (len=strlen(temp)) <= (COLS-4) ) {
		n = (COLS - len - 4) / 2;	//!< margin on both side

		go = beautify;

		/* left margin */
		if ( UPDATE == 'y' ) {
			for ( i=0; i<n; i++ ) *go++ = '-';
		} else {
			for ( i=0; i<n; i++ ) *go++ = '=';
		}
		*go++ = '<'; *go++ = ' ';

		/* title */
		strcpy(go, temp);
		while ( *go != '\0' ) go++;

		/* right margin */
		*go++ = ' '; *go++ = '>';
		if ( UPDATE == 'y' ) {
			for ( i=0; i<n; i++ ) *go++ = '-';
			*go++ = '-';	//!< one more when the n is ?.5
		} else {
			for ( i=0; i<n; i++ ) *go++ = '=';
			*go++ = '=';	//!< one more when the n is ?.5
		}

	} else {
		/* when title is longer then COLS, show only the last part */
		strcpy(beautify, temp + (len - COLS + 4));
		if ( UPDATE == 'y' )
			strcat(beautify, " >--");
		else
			strcat(beautify, " >==");
	}

	beautify[COLS] = '\0';

	switch( SELECT_PARCEL ) {
	    case 's':	/* single is selected */
            mvaddstr(DIR_LINE, 0, beautify);
            break;
	    case 'u':	/* double up */
            if ( PARCEL_MODE == 'u' || PARCEL_MODE == 'U' ) {
                attron( A_REVERSE );
                mvaddstr(DIR_LINE, 0, beautify);
                attroff( A_REVERSE );
            } else {
                mvaddstr(DIR_LINE, 0, beautify);
            }
            break;
	    case 'l':	/* double down */
            if ( PARCEL_MODE == 'l' || PARCEL_MODE == 'L' ) {
                attron( A_REVERSE );
                mvaddstr(DIR_LINE, 0, beautify);
                attroff( A_REVERSE );
            } else {
                mvaddstr(DIR_LINE, 0, beautify);
            }
            break;
	}
	move(LINES-1, 0);
}

int draw_cell(int insist)
	/* decide insistence of drawing */
			/* 1 : insist draw at UPMOST_LINE */
			/* 2 : insist draw at DOWNLAST_LINE */
			/* 0 : draw if possible on current screen */
{
	int cnt, i;
	int dsp_area;
	int real_line, real_col;

	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
            cnt = (int)(Cellnow - DSPHEAD);
            real_line = UPMOST_LINE + cnt / CELL_PER_COL;
            if ( real_line <= DOWNLAST_LINE
                && real_line >= UPMOST_LINE && cnt >= 0 ) {
                real_col = COL_WIDTH * (cnt % CELL_PER_COL);
                mvaddstr(real_line, real_col, BLANK_CELL);
                if ( Cellnow == POSITION ) {
                    /* set reverse video */
                    attron( A_UNDERLINE );
                    if ( SELECTED == 'y' ) attron( A_REVERSE );
                    mvaddstr(real_line, real_col, Cellnow->fname);
                    if ( SELECTED == 'y' ) attroff( A_REVERSE );
                    printw(CURRENT_FILE_TAG);
                    attroff( A_UNDERLINE );
                } else {
                    if ( SELECTED == 'y' ) attron( A_REVERSE );
                    mvaddstr(real_line, real_col, Cellnow->fname);
                    if ( SELECTED == 'y' ) attroff( A_REVERSE );
                }
            } else {
                switch( insist ) {
                    case 1:
                        DSPHEAD = HEAD;
                        while( Cellnow - DSPHEAD >= CELL_PER_COL )
                            DSPHEAD += CELL_PER_COL;
                        draw_parcel();
                        break;
                    case 2:
                        DSPHEAD = HEAD;
                        dsp_area = DOWNLAST_LINE - UPMOST_LINE + 1;
                        while(Cellnow-DSPHEAD >= dsp_area*CELL_PER_COL)
                            DSPHEAD += CELL_PER_COL;
                        draw_parcel();
                        break;
                    case 0:
                        return(1);	/* out of region */
                    default:
                        break;
                }
            }
            break;

	    case 'S':	/* single - long */
	    case 'U':	/* double - up - long */
	    case 'L':	/* double - low - long */
            cnt = (int)(Cellnow - DSPHEAD);
            real_line = UPMOST_LINE + cnt;
            if ( real_line <= DOWNLAST_LINE
                && real_line >= UPMOST_LINE && cnt >= 0 ) {
                draw_mini(real_line);
            } else {
                switch( insist ) {
                    case 1:
                        DSPHEAD = HEAD;
                        while( Cellnow > DSPHEAD )
                            DSPHEAD++;
                        draw_parcel();
                        break;
                    case 2:
                        DSPHEAD = HEAD;
                        dsp_area = DOWNLAST_LINE - UPMOST_LINE + 1;
                        while( Cellnow - DSPHEAD >= dsp_area)
                            DSPHEAD++;
                        draw_parcel();
                        break;
                    case 0:
                        return(1);	/* out of region */
                    default:
                        break;
                }
            }
            break;
	}
	return(0);	/* success */
}

void draw_mini(int mline)
{
	char minimsg[FILELEN * 2];
	int i, j;
	ushort modetmp, modetmpdir, modecmp = 00400;
	char perm[11], *go;
	char *timetmp;

	if ( !strcmp(Cellnow->fname, "--?--") ) {
		strcpy(minimsg, "UNKNOWN FILE  ");
	} else {
		/* Get Permition mode */
		/*	1.get perm_mode */
		/*	2.is it directory ? */
		/*	3.read, write, execute permition */
		/* 1 */
		modetmp = Cellnow->list.st_mode;
		modetmpdir = modetmp & 0070000u;
		/* 2 */
		go=perm;
		switch(modetmpdir) {
		    case S_IFDIR:
                *go = 'd'; break;
		    case S_IFBLK:
                *go = 'b'; break;
		    case S_IFCHR:
                *go = 'c'; break;
		    case S_IFIFO:
                *go = 'p'; break;
		    case S_IFLNK:
                *go = 'l'; break;
		    default:
                *go = '-';
		}
		go++;
		/* 3 */
		for ( i=0; i<3; i++ ) {
			for ( j=0; j<3; j++ ) {
				if ( modecmp & modetmp ) {
					switch ( j ) {
					    case 0:
                            *go = 'r';
                            break;
					    case 1:
                            *go = 'w';
                            break;
					    case 2:
                            *go = 'x';
                            break;
                        default:
                            break;
					}
				} else {
					*go = '-';
				}
				modecmp >>= 1u;
				go++;
			}
		}
		*go = '\0';

		/* Get Access TIME */
		timetmp = ctime(&Cellnow->list.st_mtime);
		timetmp[16] = '\0';

		/* Display MINI MESSAGE */
                sprintf(minimsg, "%10s %3d %7s %3d %14s %8ld %3ld %s"
			// "%6d ", Cellnow->list.st_ino
			, perm
			, (int)Cellnow->list.st_nlink
			, name_of_user(Cellnow->list.st_uid)
			, Cellnow->list.st_gid
			, &timetmp[4]
			, Cellnow->list.st_size
			, Cellnow->list.st_blocks
			, Cellnow->fname);
	}
	minimsg[COLS] = '\0';
	switch( PARCEL_MODE ) {
	    case 's':	/* single - normal */
	    case 'u':	/* double - up - normal */
	    case 'l':	/* double - low - normal */
            move(MINI_LINE, 0); clrtoeol();
            mvaddstr(MINI_LINE, 0, minimsg);
            break;
	    case 'S':	/* single - long */
	    case 'U':	/* double - up - long */
	    case 'L':	/* double - low - long */
            move(mline, 0); clrtoeol();
            if ( Cellnow == POSITION ) {
                attron( A_UNDERLINE );
                if ( SELECTED == 'y' ) attron( A_REVERSE );
                mvaddstr(mline, 0, minimsg);
                if ( SELECTED == 'y' ) attroff( A_REVERSE );
                if ( strlen(minimsg) < COLS ) printw(CURRENT_FILE_TAG);
                attroff( A_UNDERLINE );
            } else {
                if ( SELECTED == 'y' ) attron( A_REVERSE );
                mvaddstr(mline, 0, minimsg);
                if ( SELECTED == 'y' ) attroff( A_REVERSE );
            }
            break;
	}
}
/***************************************************************************
**	mud_change	#mud_change
************1***************************************************************/
void mud_change()
{
	char message[64];
	char temp_title[TITLELEN];
	int save_mudnum, i;
	int ret;

	/* update name of current mud */
	name_mud();

	save_mudnum = Mud_num;
	clear();
	for ( i=0; i<MUD_MAX_CNT; i++ ) {
		Mudnow = Muds[i];
		if ( Mudnow == (MUD *)NULL ) {
			sprintf(message, "Mud %c: Not Allocated", 'a'+i);
		} else {
			if ( !strcmp(TITLE, "") ) {
				sprintf(message
				, "Mud %c: No Title", 'a'+i);
			} else {
				sprintf(message
				, "Mud %c: \"%s\"", 'a'+i, TITLE);
			}
		}
		mvaddstr(i+(LINES-MUD_MAX_CNT)/2, 0, message);
		if ( i == save_mudnum ) printw(CURRENT_FILE_TAG);
	}
#if 1
	printerr("Enter mud charater which you want"); rrr();
	ret = getchar();
#else
	printerr("Enter mud charater which you want (t:change_title)"); rrr();
	ret = getchar();
	if ( ret == 't' ) {
		Mudnow = Muds[save_mudnum];
		if ( Mudnow != (MUD *)NULL ) {
			get_str(TITLE, "Enter new Title : ", TITLELEN);
			mud_change(); return;
		} else {
			printerr("This Mud not allocated, try again"); rrr();
			ret = getchar();
		}
	}
#endif
	ret -= 'a';
	if ( ret < 0 || ret >= MUD_MAX_CNT ) {
		printerr("Back to the previous Mud"); rrr();
		Mud_num = save_mudnum; start_mud(""); return;
	}
	Mud_num = ret;
	Mudnow = Muds[Mud_num];
	if ( Mudnow == (MUD *)NULL || !strcmp(TITLE, "") ) {
		/* Just open new mud with dummy name.
		 * User may change name later.
		get_str(temp_title, "Enter Title : ", TITLELEN);
		 */
		strcpy(temp_title, "---");
	}
	start_mud(temp_title);
}
/***************************************************************************
**	start_mud	#start_mud
***************************************************************************/
void start_mud(char* mud_name)
{
	if ( Muds[Mud_num] == (MUD *)NULL ) {
		switch( get_newmud() ) {
		    case 1:	/* cannot get memory for Mudnow */
                printerr2("cannot get Memory for Muds.");
                exit(1);
		    case 0:	/* success */
                /* set Mudnow to Muds[i] */
                Muds[Mud_num] = Mudnow;
                strcpy(TITLE, mud_name);
                break;
		}
		Parcelnow = SPARCEL;
		switch( get_newparcel(Startupdir, 'n') ) {
		    case 1:	/* cannot get memory for Parcelnow */
                printerr2("Cannot get memory for Parcelnow");
                exit(1);
		    case 2:	/* cannot open directory */
                printerr2("Cannot open directory");
                free(Parcelnow);
                exit(1);
		    case 3:	/* cannot get memory for CELLs */
                printerr2("Cannot get memory for cells");
                free(Parcelnow);
                exit(1);
		    case 0:	/* success */
                /* decide parcel_mode
                 * & dir_line,mini_line,upmost_line,downlast_line
                 */
                SPARCEL = Parcelnow;
                PARCEL_MODE = 's';
                parcel_line();
                break;
		}
		draw_parcel();
	} else {
		Mudnow = Muds[Mud_num];
		if ( !strcmp(TITLE, "") ) strcpy(TITLE, mud_name);
		redraw();
	}
}
/***************************************************************************
**	change directory #change_dir
***************************************************************************/
void change_dir()
{
	char filename[FILELEN];
	char absolute[DIRLEN];
	char save_cwd[DIRLEN];
	char new_cwd[DIRLEN];
	ushort modetmp, modedir;

	/* check unknown file */
	if ( !strcmp(POSITION->fname, "--?--") ) {
		printerr("UNKNOWN FILE."); rrr(); return;
	}
	/* cp POSITION to filename */
	strcpy(filename, POSITION->fname);
	remove_tail(filename);
	/* check case "." & ".." */
	save_cwd[0] = '\0';
	if ( !strcmp(filename, ".") ) {
		printerr("Use \"R\" for re-read current directory."); rrr();
		return;
	} else if ( !strcmp(filename, "..") && !strcmp(CURRENT_DIR, "/") ) {
		printerr("Current Dir is ROOT"); rrr(); return;
	} else if ( !strcmp(filename, "..") && strcmp(CURRENT_DIR, "/") ) {
		char *go;
		go = CURRENT_DIR;
		while ( *go != '\0' ) go++;
		while ( *go != '/' ) go--; go++;
		strcpy(save_cwd, go);
		go = save_cwd;
		while ( *go != '\0' ) go++;
		strcpy(go, "/");
	}
	/* case "Not a directory" */
	Cellnow = POSITION;
	modetmp = Cellnow->list.st_mode;
	modedir = modetmp & S_IFMT;
	if ( modedir != S_IFDIR && modedir != S_IFLNK ) {
		printerr("It's not a directory."); rrr(); return;
	}
	/* prepare next for cwd */
	strcpy(absolute, CURRENT_DIR);
	switch( adjust_dir(absolute, filename, new_cwd) ) {
	    case 1:	/* abs not abs_form */
	    case 2:	/* can't open save */
	    case 3:	/* save is not a dir */
		printerr("Error on change directory"); rrr(); return;
	}
	re_read(new_cwd, 0);
	/* keep position on previous dir */
	if ( save_cwd[0] != '\0' ) {
		CELL *go_cells;
		int i, leng;
		leng = (int)strlen(save_cwd);
		for ( go_cells = HEAD; go_cells <= TAIL; go_cells++ ) {
			for ( i=0; i<leng; i++ ) {
				if ( *(save_cwd+i) != *(go_cells->fname+i) )
					break;
			}
			if ( i == leng ) break;
		}
		if ( go_cells > TAIL ) POSITION = DSPHEAD;
		else POSITION = go_cells;
	}
	draw_parcel();
}
void cd_argu()
{
	char new_dir[DIRLEN];

	/* get new directory */
	get_str(new_dir, "Enter new directory : ", DIRLEN);
	if ( new_dir[0] == '\0' ) {
		printerr(""); rrr(); return;
	} else {
		char absolute[DIRLEN];
		char save_new[DIRLEN];
		strcpy(absolute, CURRENT_DIR);
		switch( adjust_dir(absolute, new_dir, save_new) ) {
		    case 1:	/* abs not abs_form */
			printerr("Error on change dir"); rrr(); return;
		    case 2:	/* can't open save */
			printerr("Cannot open directory"); rrr(); return;
		    case 3:	/* save is not a dir */
			printerr("Invalide directory"); rrr(); return;
		    case 0:	/* success */
			strcpy(new_dir, save_new);
			break;
		}
	}
	re_read(new_dir, 1);
}
void remove_tail(char* filename)
{
	if ( ! filename )
		return;

	while ( *filename && ! strchr(FNAME_TAILES, *filename) )
		filename++;

	*filename = '\0';
}

/**	@brief	Get sample string from original string
 *
 *	This function takes a sample from original string.
 *	The result string should not greater than nMax.
 */
char* sample_string(char* pOrig, int nMax)
{
	int nLen;
	char* pRet;

	if ( ! pOrig || ! *pOrig || nMax < 5 )
		return "";

	nLen = (int)strlen(pOrig);

	if ( nLen <= nMax )
		return pOrig;

	pRet = pOrig + (nLen - nMax);
	pRet[0] = '.';
	pRet[1] = '.';
	pRet[2] = '.';

	return pRet;
}

/**	@brief	Name the Mud with directory name
 */
void name_mud(void)
{
	char szUpDir[DIRLEN];
	char szDnDir[DIRLEN];

	switch( SELECT_PARCEL ) {
	    case 's':	/* single is selected */
		if ( ! Mudnow || ! SPARCEL ) break;
		strncpy(szUpDir, SPARCEL->current_dir, DIRLEN);
		sprintf(TITLE, "%s", sample_string(szUpDir, 60));
		break;

	    case 'u':	/* double up */
	    case 'l':	/* double down */
		if ( ! Mudnow || ! DUPARCEL || ! DLPARCEL ) break;
		strncpy(szUpDir, DUPARCEL->current_dir, DIRLEN);
		strncpy(szDnDir, DLPARCEL->current_dir, DIRLEN);
		sprintf(TITLE, "%s | %s",
			sample_string(szUpDir, 30), sample_string(szDnDir, 30));
		break;
	}
}

/***************************************************************************
**	re-read parcel		#re_read
***************************************************************************/
void re_read(char* pathname, int redraw_flag)
{
	PARCEL *save_parcel;
	char save_parcel_mode;
	char save_sort_method;

	save_parcel = Parcelnow;
	save_parcel_mode = PARCEL_MODE;
	save_sort_method = SORT_METHOD;
	switch( get_newparcel(pathname, save_sort_method) ) {
	    case 2:	/* cannot open directory */
	    case 3:	/* cannot get memory for CELLs */
		free(Parcelnow);
	    case 1:	/* cannot get memory for Parcelnow */
		Parcelnow = save_parcel;
		printerr("\"Error on re-read. Back to Pre-\". Press Return");
		rrr(); getchar(); break;
	    case 0:	/* success */
		/* decide parcel_mode
		 * & dir_line,mini_line,upmost_line,downlast_line
		 */
		PARCEL_MODE = save_parcel_mode;
		SORT_METHOD = save_sort_method;
		switch( PARCEL_MODE ) {
		    case 's':	/* single - normal */
		    case 'S':	/* single - long */
                SPARCEL = Parcelnow; break;
		    case 'u':	/* double - up - normal */
		    case 'U':	/* double - up - long */
                DUPARCEL = Parcelnow; break;
		    case 'l':	/* double - low - normal */
		    case 'L':	/* double - low - long */
                DLPARCEL = Parcelnow; break;
		}
		parcel_line();
		free(save_parcel->head);
		free(save_parcel);
		break;
	}
	if( redraw_flag ) draw_parcel();
}

/***************************************************************************
**	Get directory entries	#get_newparcel
***************************************************************************/
#ifdef	USAGE
		switch( get_newparcel(pathname, sort_meth) ) {
		    case 1:	/* cannot get memory for Parcelnow */
			break;
		    case 2:	/* cannot open directory */
			free(Parcelnow);
			break;
		    case 3:	/* cannot get memory for CELLs */
			free(Parcelnow);
			break;
		    case 0:	/* success */
			/* decide parcel_mode
			 * & dir_line,mini_line,upmost_line,downlast_line
			 */
			break;
		}
#endif
int get_newparcel(char* pathname, char sort_meth)
{
	char fullname[DIRLEN];
	int save_dsphead, save_position;
	int cnt = 0, i, max_fname_len = 0, length;
	DIR *dirp;
	struct dirent *dp;

	/* 0 : maintain DSPHEAD */
	save_dsphead = 0;
	save_position = 0;
	if ( Parcelnow != (PARCEL *)NULL ) {
		if ( !strcmp(CURRENT_DIR, pathname) ) {
			save_dsphead = DSPHEAD - HEAD;
			save_position = POSITION - HEAD;
		}
	}
	printerr("Wait"); rrr();
	/* 1 : get_parcel & init it */
	if ((Parcelnow = (PARCEL *)malloc(sizeof(PARCEL))) == (PARCEL *)NULL) {
		return(1);
	}
	SORT_METHOD = sort_meth;

	/* 2 : open dir */
	strcpy(CURRENT_DIR, pathname);
	if ( (dirp = opendir(CURRENT_DIR)) == (DIR *)NULL ) {
		return(2);	/* cannot open directory */
	}

	/* 3 : first read, decide parameter size */
	while ( (dp = readdir(dirp)) != (struct dirent *)NULL ) {
		cnt++;
		if ( (length = strlen(dp->d_name)) > max_fname_len )
			max_fname_len = length;
	}
	max_fname_len += 5;
	CELL_PER_COL = COLS / max_fname_len > 1 ? COLS / max_fname_len : 1;
	COL_WIDTH = max_fname_len;
	NUM_OF_CELL = cnt;
	for ( i=0; i<COL_WIDTH; i++ ) Parcelnow->blank_cell[i] = ' ';
	Parcelnow->blank_cell[i] = '\0';
	/* 4 : get memory, init it */
	if ( (HEAD = (CELL *)calloc(cnt, sizeof(CELL))) == (CELL *)NULL ) {
		return(3);	/* cannot get memory */
	}
	for ( i=0; i<NUM_OF_CELL; i++ ) {
		Cellnow = HEAD + i;
		SELECTED = 'n';
	}
	/* 5 : read directory again, and save it to data-base */
	rewinddir(dirp);
	cnt = 0;
	while ( (dp = readdir(dirp)) != (struct dirent *)NULL ) {
		Cellnow = HEAD + cnt;
		strcpy(Cellnow->fname, dp->d_name);
		sprintf(fullname, "%s/%s", CURRENT_DIR, dp->d_name);
		get_file_status(fullname);
		cnt++;
	}
	TAIL = HEAD + (NUM_OF_CELL - 1);
	/* 6 : close directory & finish */
	closedir(dirp);
	qsort_cell(HEAD, TAIL);
	TAIL = HEAD + (NUM_OF_CELL - 1);
	DSPHEAD = HEAD;
	POSITION = HEAD;
	UPDATE = 'y';
	/* 7 : maintain DSPHEAD */
	if ( save_dsphead ) {
		DSPHEAD = HEAD + save_dsphead;
		if ( DSPHEAD > TAIL ) DSPHEAD = TAIL;
	}
	if ( save_position ) {
		POSITION = HEAD + save_position;
		if ( POSITION > TAIL ) POSITION = TAIL;
	}

	return(0);
}

void get_file_status(char* name)
{
	struct stat* pStat = (void*)&Cellnow->list;

	if ( lstat(name, pStat) < 0 ) {
		strcat(Cellnow->fname, "?");
		return;
	}

	if ( (pStat->st_mode & S_IFMT) == S_IFIFO )
		strcat(Cellnow->fname, "|");
	else if ( (pStat->st_mode & S_IFMT) == S_IFDIR )
		strcat(Cellnow->fname, "/");
	else if ( (pStat->st_mode & S_IFMT) == S_IFLNK )
		strcat(Cellnow->fname, "@");
	else if ( (pStat->st_mode & S_IFMT) == S_IFSOCK )
		strcat(Cellnow->fname, "=");
	else if ( pStat->st_mode & S_IXUSR )
		strcat(Cellnow->fname, "*");
}

void qsort_cell(CELL* left_edge, CELL* right_edge)
{
	if(left_edge < right_edge) {
		CELL *left, *right, *middle;

		middle = left_edge;
		left = left_edge;
		right = right_edge + 1;

		do {
			do {
				left++;
				if ( left > right_edge ) {
					left--;
					break;
				}
			} while ( str_cmp(left, middle) <= 0 );
			do {
				right--;
				if ( right < left_edge ) {
					right++;
					break;
				}
			} while ( str_cmp(middle, right) < 0 );
			if ( left < right ) {
				swap_cell(left, right);
			}
		} while ( left < right );
		swap_cell(middle, right);
		qsort_cell(left_edge, right-1);
		qsort_cell(right+1, right_edge);
	}
}

int str_cmp(CELL* AAA, CELL* BBB)
{
	char *aaa, *bbb;
	char aname[FILELEN], bname[FILELEN];
	int ret;

    switch (SORT_METHOD)
    {
        case 'n':
            strcpy(aname, AAA->fname);
            strcpy(bname, BBB->fname);
            remove_tail(aname);
            remove_tail(bname);
            return ((int) strcmp(aname, bname));
        case 't':
            return ((int) (BBB->list.st_mtime - AAA->list.st_mtime));
        case 'e':
            strcpy(aname, AAA->fname);
            strcpy(bname, BBB->fname);
            remove_tail(aname);
            remove_tail(bname);

            /* decide extention aaa */
            aaa = aname;
            while (*aaa != '\0') aaa++;
            while (*aaa != '.' && aaa != aname) aaa--;
            if (aaa == aname)
            {
                while (*aaa != '\0') aaa++;
            }
            else
            {
                aaa++;
            }
            /* decide extention bbb */
            bbb = bname;
            while (*bbb != '\0') bbb++;
            while (*bbb != '.' && bbb != bname) bbb--;
            if (bbb == bname)
            {
                while (*bbb != '\0') bbb++;
            }
            else
            {
                bbb++;
            }

            if ((ret = strcmp(aaa, bbb)) == 0)
            {
                return ((int) strcmp(aname, bname));
            }
            else
            {
                return (ret);
            }
        case 'd':
            aaa = AAA->fname;
            bbb = BBB->fname;
            while (*aaa && !strchr(FNAME_TAILES, *aaa)) aaa++;
            while (*bbb && !strchr(FNAME_TAILES, *bbb)) bbb++;
            if ((ret = strcmp(aaa, bbb)) == 0)
            {
                strcpy(aname, AAA->fname);
                strcpy(bname, BBB->fname);
                remove_tail(aname);
                remove_tail(bname);
                return ((int) strcmp(aname, bname));
            }
            else
            {
                return (-ret);
            }
    }
}

void swap_cell(CELL* AAA, CELL* BBB)
{
	CELL TTT;

	TTT = *BBB;
	*BBB = *AAA;
	*AAA = TTT;
}

/*****************************************************************************
**	sort_method	#sort_method
*****************************************************************************/
void sort_method()
{
	char msg[80];
	sprintf(msg,
	"[ %c ] Sort Key : [n:name],[t:time],[e:extention],[d:directory]"
	, SORT_METHOD);
	printerr(msg); rrr();
	switch( getchar() ) {
	    case 't':	/* time */
		SORT_METHOD = 't'; break;
	    case 'e':	/* extention */
		SORT_METHOD = 'e'; break;
	    case 'd':	/* directory & executable */
		SORT_METHOD = 'd'; break;
	    case 'n':	/* name */
		SORT_METHOD = 'n'; break;
	    default:
		printerr("Sort Key is NOT changed."); rrr(); return;
	}
	sprintf(msg, "[ %c ] Press \"R\" to sort again", SORT_METHOD);
	printerr(msg); rrr();
}

/*****************************************************************************
**	decide PARCEL lines	#parcel_line
*****************************************************************************/
#ifdef	USAGE
		1. select parcel
		2. change parcel_mode
		3. call parcel_line()
#endif
void parcel_line()
{
	switch( PARCEL_MODE ) {
	    case 's':
		DIR_LINE = 0;
		MINI_LINE = 1;
		UPMOST_LINE = 2;
		DOWNLAST_LINE = LINES - 2;
		break;
	    case 'S':
		DIR_LINE = 0;
		UPMOST_LINE = 1;
		DOWNLAST_LINE = LINES -2;
		break;
	    case 'u':
		DIR_LINE = 0;
		MINI_LINE = 1;
		UPMOST_LINE = 2;
		DOWNLAST_LINE = Sep_line - 1;
		break;
	    case 'U':
		DIR_LINE = 0;
		UPMOST_LINE = 1;
		DOWNLAST_LINE = Sep_line - 1;
		break;
	    case 'l':
		DIR_LINE = Sep_line;
		MINI_LINE = Sep_line + 1;
		UPMOST_LINE = Sep_line + 2;
		DOWNLAST_LINE = LINES - 2;
		break;
	    case 'L':
		DIR_LINE = Sep_line;
		UPMOST_LINE = Sep_line + 1;
		DOWNLAST_LINE = LINES - 2;
		break;
	}
}
/****************************************************************************
**	get new mud	#get_newmud
****************************************************************************/
#ifdef USAGE
		switch( get_newmud() ) {
		    case 1:	/* cannot get memory for Mudnow */
			break;
		    case 0:	/* success */
			/* set Mudnow to Muds[i] */
			break;
		}
#endif
int get_newmud()
{
	if ( (Mudnow = (MUD *)malloc(sizeof(MUD))) == (MUD *)NULL )
		return(1);
	/* init Mudnow */
	SELECT_PARCEL = 's';
	SPARCEL = (PARCEL *)NULL;
	DUPARCEL = (PARCEL *)NULL;
	DLPARCEL = (PARCEL *)NULL;
	strcpy(TITLE, "");
	strcpy(SEARCH_PATTERN, "");
	return(0);
}
/*****************************************************************************
**	for message	#printerr
*****************************************************************************/
void printerr(char* msg)
{
	char message[512];
	sprintf(message, ": %s", msg);
	message[COLS-1] = '\0';
	move(LINES-1, 0); clrtoeol();
	mvaddstr(LINES-1, 0, message);
}

void printerr2(char* msg)
{
	mvaddstr(LINES/2, 0, msg);
	refresh();
	endwin();
}

/**	@brief	Move cursor to default position, and refresh
 */
int rrr(void)
{
	move(LINES-1, 0);
	refresh();
	return 0;
}

void get_str(char* temp, char* msg, int len)
{
	int save_len;
	char *go, ret;
	save_len = len;
	move(LINES-1, 0); clrtoeol();
	move(LINES-1, 0); printw("%s", msg); refresh();
	go = temp;
	while ( (ret = getchar()) != '\r' && ret != '\n' && len > 5 ) {
		if ( ret == '\b' ) {
			if ( go > temp ) go--, printw("%c", ret), refresh();
			if ( len < save_len ) len++;
		} else {
			len--;
			*go = ret; go++;
			printw("%c", ret); refresh();
		}
	}
	*go = '\0';
}
/****************************************************************************
**	search	#search
****************************************************************************/
void search_fname(int insist)
{
	CELL *save_cell;
	if ( insist ) get_str(SEARCH_PATTERN, "/", FILELEN);
	else {
		if ( !strcmp(SEARCH_PATTERN, "") ) {
			printerr("No previous pattern. use \"/\""); rrr();
			return;
		}
	}
	Cellnow = POSITION;
	switch ( search(SEARCH_PATTERN) ) {
	    case 1: /* not found */
		printerr("pattern not found"); rrr(); break;
	    case 0: /* found, Cellnow is that CELL */
		save_cell = Cellnow;
		Cellnow = POSITION; POSITION = save_cell; draw_cell(0);
		Cellnow = POSITION; draw_cell(1);
		switch( PARCEL_MODE ) {
		    case 's': case 'u': case 'l': /* normal */
			draw_mini(MINI_LINE);
			break;
		    case 'S': case 'U': case 'L': /* long */
			break;
		}
		rrr(); break;
	}
}
#ifdef USAGE
		set Cellnow as a start point
		switch ( search(pattern) ) {
		    case 1: /* not found */
		    case 0: /* found, Cellnow is that CELL */
		}
#endif
int search(char* AAA)
{
	char search_str[FILELEN];
	CELL *go_cell;

	strcpy(search_str, AAA);
	go_cell = Cellnow;

	do {
		if ( go_cell == TAIL ) {
			go_cell = HEAD;
		} else {
			go_cell++;
		}
		if ( search_cmp(search_str, go_cell->fname) ) break;
	} while ( go_cell != Cellnow );

	if ( go_cell == Cellnow ) {
		if ( !search_cmp(search_str, go_cell->fname) )
			return(1);	/* not found */
	}
	Cellnow = go_cell;
	return(0);
}
int search_cmp(char* AAA, char* BBB) /* AAA:search_str BBB:object */
	/* return 1:found 0:not */
{
	int leng_A, leng_B;
	leng_A = (int)strlen(AAA);
	leng_B = (int)strlen(BBB);
	if ( leng_A > leng_B ) {
		return(0);
	} else {
		int i, j, diff_leng;
		diff_leng = leng_B - leng_A;
		for ( i=0; i<=diff_leng; i++ ) {
			for ( j=0; j<leng_A; j++ ) {
				if ( *(AAA+j) != *(BBB+(i+j)) ) break;
			}
			if ( j == leng_A ) return(1);
		}
		return(0);
	}
}
/***************************************************************************
**	shell command		#shell_cmd
***************************************************************************/
void shell_cmd()
{
	char temp_cmd[DIRLEN];
	endwin();
	printf("\n\nType \"exit\" to come back\n");
	printf("cwd = %s\n", CURRENT_DIR);
	fflush(stdout);
	sprintf(temp_cmd, "cd \"%s\"; PS1=\"%s:%s:abc> \"; export PS1; sh"
		, CURRENT_DIR, Hostname, Username);
	system(temp_cmd);
	cbreak(); noecho(); nonl();
	UPDATE = 'n';
	redraw();
}
/***************************************************************************
**	help_me		#help_me
***************************************************************************/
void help_me()
{
	char **go;
	int i;
	static char *page[] = {
(char *)1,
"1. Move cursor commands",
"  : These commands are similar to \"vi\" commands",
" ",
"    \"j\", \"DOWN\"  : move down cursor",
"    \"k\", \"UP\"    : move up cursor",
"    \"h\", \"LEFT\"  : move left cursor,  0: to first column",
"    \"l\", \"RIGHT\" : move right cursor, $: to last column",
" ",
"    \"^F\" : move down one page",
"    \"^B\" : move up one page",
"    \"^E\" : move down one line",
"    \"^Y\" : move up one line",
" ",
"    \"H\" : move to the upmost line of this screen",
"    \"M\" : move to the middle line of this screen",
"    \"L\" : move to the downlast line of this screen",
" ",
"    \"G\" : move to the end of list",
"          : \"n + G\" : move to the n'th file_name of list",
"    \"^G\" : print current position in the list",
(char *)1,
"2. Control list commands",
"  : These commands controls list.",
" ",
"    \"q\" : quit ABC program",
" ",
"  : Mud - a set of lists : ABC reserves 19 Muds",
"  : List - a set of file_informations",
"  : Single Mode - one list shown on the screen",
"  : Double Mode - two lists shown on the screen. UPPER & LOWER",
"    \"m\" : get new Mud",
"          : First, new Mud show you your Startup directory",
"    \"~\" : Mode change, Single to Double, Double to Single",
"    \"|\" : Toggle UPPER & LOWER in Double Mode",
"    \",\" : Toggle normal-list & long-list of list",
" ",
"    \"r\", \"^L\" : refresh this list",
"    \"R\" : re-read directory information from disk",
" ",
"    \"T\" : change Sort-key of this list",
(char *)1,
"2. Control list commands ( continue )",
"    \"c\", \"RETURN\" : change directory to current file",
"    \"C\" : jump to the new directory",
" ",
"    \"/\" : search pattern in the list",
"    \"n\" : search pattern repeatly",
" ",
"    \"!\" : shell command, prompt is \"HOSTNAME:username:abc > \"",
"    \"?\" : help for ABC",
" ",
"3. Select files commands",
"   : You can make a group of files with selecting files",
"     And some command act on the group of files",
" ",
"    \"i\" : select current file",
"    \"u\" : unselect current file",
"    \"I\" : print number of files in group",
"    \"+\" : select all file in this list",
"    \"-\" : unselect all file in this list",
(char *)1,
"    \"e\" : edit current file",
"    \"s\" : see current file, cannot edit",
"    \"%\" : compare directory entries",
"    \"==\" : compare same filename",
"    \"=_\" : compare selected filename",
" ",
"4. Dangerous commands",
"   : Dangerous commands are copy, move, delete, edit ...",
"     type \"26p\" to get the \"Dangerous commands Mode\"",
" ",
"    \"c\" : copy file(s) to a target",
"    \"C\" : copy file(s) to the opposite side in double mode",
" ",
"    \"m\" : move file(s) to a target",
"    \"M\" : move file(s) to the opposite side in double mode",
" ",
"    \"d\" : delete file(s)",
"    \"D\" : delete file(s) & directory(s), POWERFUL DELETE, be careful",
" ",
"    \"^\" : cp directory, include all sub directory",
(char *)NULL
	};

	for ( go = page, i=0; ; go++, i++ ) {
		if ( *go == (char *)1 ) {
			printerr("Press any key to next help"); rrr();
			getchar(); clear(); i = 0;
		} else if ( *go == (char *)NULL ) {
			printerr("Press any key to ABC"); rrr();
			getchar(); clear(); redraw();
			return;
		} else {
			mvaddstr(i, 0, *go);
		}
	}
}
/****************************************************************************
**	dangerous commands 	#danger
****************************************************************************/
void edit_file(char flag)
{
	char ed_cmd[DIRLEN];
	char filename[FILELEN];
	endwin();
	strcpy(filename, POSITION->fname); remove_tail(filename);
	switch ( flag ) {
	    case 'e':
            sprintf(ed_cmd, "cd \"%s\"; vi \"%s\"", CURRENT_DIR, filename);
            break;
	    case 's':
            sprintf(ed_cmd, "cd \"%s\"; view \"%s\"", CURRENT_DIR, filename);
            break;
        default:
            break;
	}
	system(ed_cmd);
	cbreak(); nonl(); noecho();
	UPDATE = 'n';
	redraw();
}

void delete_files(char flag)
{
	int file_num = 0;
	char del_cmd[DIRLEN];
	char filename[FILELEN];

	endwin();
	file_num = check_selected_files();
	switch( flag ) {
	    case 'd':
            printf("\n\n##### DELETE FILES\n"); break;
	    case 'D':
            printf("\n\n##### DELETE FILES AND DIRECTORIES\n"); break;
        default:
            break;
	}
	if ( file_num ) {
		printf("### [ %d ] files or directories selected.\n", file_num);
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) printf("%s  ", Cellnow->fname);
		}
		printf("\n");
	} else {
		printf("### [ %s ] is selected.\n", POSITION->fname);
	}
	printf("### Delete files (y/n) ? : "); fflush(stdout);
	if ( getchar() != 'y' ) {
		cbreak(); nonl(); noecho();
		redraw();
		printerr("delete files STOP."); rrr(); return;
	}
	while ( getchar() != '\n' ) ;
	if ( file_num > 1 ) {
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) {
				strcpy(filename, Cellnow->fname);
				remove_tail(filename);
                switch( flag ) {
                    case 'd':
                        printf("%s --> deleted\n", Cellnow->fname);
                        fflush(stdout);
                        sprintf(del_cmd, "cd \"%s\"; rm \"%s\""
                            , CURRENT_DIR, filename);
                        break;
                    case 'D':
                        printf("%s ==> DELETED\n", Cellnow->fname);
                        fflush(stdout);
                        sprintf(del_cmd, "cd \"%s\"; rm -r \"%s\""
                            , CURRENT_DIR, filename);
                        break;
                    default:
                        break;
                }
				system(del_cmd);
			}
		}
	} else if ( file_num == 1 ) {
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) break;
		}
		strcpy(filename, Cellnow->fname); remove_tail(filename);
		switch( flag ) {
		    case 'd':
                printf("%s --> deleted\n", Cellnow->fname);
                fflush(stdout);
                sprintf(del_cmd, "cd \"%s\"; rm \"%s\"", CURRENT_DIR, filename);
                break;
		    case 'D':
                printf("%s ==> DELETED\n", Cellnow->fname);
                fflush(stdout);
                sprintf(del_cmd, "cd \"%s\"; rm -r \"%s\"", CURRENT_DIR, filename);
                break;
            default:
                break;
		}
		system(del_cmd);
	} else {
		strcpy(filename, POSITION->fname); remove_tail(filename);
		switch( flag ) {
		    case 'd':
                printf("%s --> deleted\n", POSITION->fname);
                fflush(stdout);
                sprintf(del_cmd, "cd \"%s\"; rm \"%s\"", CURRENT_DIR, filename);
                break;
		    case 'D':
                printf("%s ==> DELETED\n", POSITION->fname);
                fflush(stdout);
                sprintf(del_cmd, "cd \"%s\"; rm -r \"%s\"", CURRENT_DIR, filename);
                break;
            default:
                break;
		}
		system(del_cmd);
	}
	printf("### delete files Completed. Press return : "); fflush(stdout);
	getchar();
	cbreak(); nonl(); noecho();
	re_read(CURRENT_DIR, 0);
	redraw();
}

void over_send(char flag)
	/* C:copy M:move */
{
	int file_num = 0;
	char target[DIRLEN];
	char cp_cmd[DIRLEN];
	char filename[FILELEN];

	switch ( SELECT_PARCEL ) {
	    case 's':
		printerr(""); rrr(); return;
	    case 'u':
		Parcelnow = DLPARCEL; strcpy(target, CURRENT_DIR);
		Parcelnow = DUPARCEL; break;
	    case 'l':
		Parcelnow = DUPARCEL; strcpy(target, CURRENT_DIR);
		Parcelnow = DLPARCEL; break;
	}
	endwin();
	file_num = check_selected_files();
	switch( flag ) {
	    case 'C':
            printf("\n\n##### COPY FILES\n"); break;
	    case 'M':
            printf("\n\n##### MOVE FILES\n"); break;
        default:
            break;
	}
	if ( file_num ) {
		printf("### [ %d ] files selected.\n", file_num);
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) printf("%s  ", Cellnow->fname);
		}
		printf("\n");
	} else {
		printf("### [ %s ] is selected.\n", POSITION->fname);
	}
	printf("### Target dir = %s\n", target);
	printf("### Continue (y/n) ? : "); fflush(stdout);
	if ( getchar() != 'y' ) {
		cbreak(); nonl(); noecho();
		redraw();
		printerr("over send STOP."); rrr(); return;
	}
	while ( getchar() != '\n' ) ;
	if ( file_num > 1 ) {
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) {
				strcpy(filename, Cellnow->fname);
				remove_tail(filename);
                switch( flag ) {
                    case 'C':
                        printf("%s --> %s\n", Cellnow->fname, target);
                        fflush(stdout);
                        sprintf(cp_cmd, "cd \"%s\"; cp \"%s\" \"%s\"", CURRENT_DIR, filename, target);
                        break;
                    case 'M':
                        printf("%s ==> %s\n", Cellnow->fname, target);
                        fflush(stdout);
                        sprintf(cp_cmd, "cd \"%s\"; mv \"%s\" \"%s\"", CURRENT_DIR, filename, target);
                        break;
                    default:
                        break;
                }
				system(cp_cmd);
			}
		}
	} else if ( file_num == 1 ) {
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) break;
		}
		strcpy(filename, Cellnow->fname); remove_tail(filename);
		switch( flag ) {
		    case 'C':
                printf("%s --> %s\n", Cellnow->fname, target);
                fflush(stdout);
                sprintf(cp_cmd, "cd \"%s\"; cp \"%s\" \"%s\"", CURRENT_DIR, filename, target);
                break;
		    case 'M':
                printf("%s ==> %s\n", Cellnow->fname, target);
                fflush(stdout);
                sprintf(cp_cmd, "cd \"%s\"; mv \"%s\" \"%s\"", CURRENT_DIR, filename, target);
                break;
            default:
                break;
		}
		system(cp_cmd);
	} else {
		strcpy(filename, POSITION->fname); remove_tail(filename);
		switch( flag ) {
		    case 'C':
                printf("%s --> %s\n", POSITION->fname, target);
                fflush(stdout);
                sprintf(cp_cmd, "cd \"%s\"; cp \"%s\" \"%s\"", CURRENT_DIR, filename, target);
                break;
		    case 'M':
                printf("%s ==> %s\n", POSITION->fname, target);
                fflush(stdout);
                sprintf(cp_cmd, "cd \"%s\"; mv \"%s\" \"%s\"", CURRENT_DIR, filename, target);
                break;
            default:
                break;
		}
		system(cp_cmd);
	}
	printf("### send over is Completed. Press return : "); fflush(stdout);
	getchar();
	cbreak(); nonl(); noecho();
	switch ( SELECT_PARCEL ) {
	    case 'u':
            Parcelnow = DLPARCEL; re_read(CURRENT_DIR, 0);
            Parcelnow = DUPARCEL; re_read(CURRENT_DIR, 0); break;
	    case 'l':
            Parcelnow = DUPARCEL; re_read(CURRENT_DIR, 0);
            Parcelnow = DLPARCEL; re_read(CURRENT_DIR, 0); break;
	}
	redraw();
}

void over_comp(char flag)
	/* =: comp same file, _: comp diff file */
{
	char target[DIRLEN];
	char target_file[FILELEN];
	char comp_cmd[DIRLEN];
	char filename[FILELEN];

	/* check flag */
	switch ( flag ) {
	case '=':
	case '_':
		break;
	default:
		printerr(""); rrr(); return;
	}

	/* get target dir & file name */
	switch ( SELECT_PARCEL ) {
	    case 's':
            printerr(""); rrr(); return;
	    case 'u':
            Parcelnow = DLPARCEL;
            strcpy(target, CURRENT_DIR);
            strcpy(target_file, POSITION->fname);
            Parcelnow = DUPARCEL;
            break;
	    case 'l':
            Parcelnow = DUPARCEL;
            strcpy(target, CURRENT_DIR);
            strcpy(target_file, POSITION->fname);
            Parcelnow = DLPARCEL;
            break;
	}

	endwin();

	/* execute command */
	switch ( flag ) {
	case '=':
		sprintf(comp_cmd, "diff \"%s/%s\" \"%s/%s\" | less",
			CURRENT_DIR, POSITION->fname, target, POSITION->fname);
		break;
	case '_':
		sprintf(comp_cmd, "diff \"%s/%s\" \"%s/%s\" | less",
			CURRENT_DIR, POSITION->fname, target, target_file);
		break;
    default:
        break;
	}
	system(comp_cmd);

	cbreak(); nonl(); noecho();
	redraw();
}

void copy_files()
{
	int file_num = 0;
	char target[DIRLEN];
	char cp_cmd[DIRLEN];
	char filename[FILELEN];

	endwin();
	file_num = check_selected_files();
	printf("\n\n##### COPY FILES\n");
	if ( file_num ) {
		printf("### [ %d ] files selected.\n", file_num);
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) printf("%s  ", Cellnow->fname);
		}
		printf("\n");
	} else {
		printf("### [ %s ] is selected.\n", POSITION->fname);
	}
	printf("### Enter [target_dir] or [target_file]\n");
	printf("TO : "); fflush(stdout);
	fgets(target, DIRLEN, stdin); rtrim(target);
	if ( file_num > 1 ) {
		int ret;
		char temp_save[DIRLEN];
		if ( (ret = adjust_dir(CURRENT_DIR, target, temp_save)) ) {
			cbreak(); nonl(); noecho();
			redraw();
			switch( ret ) {
			    case 2:	/* can't open save */
                    printerr("STOP. Cannot open Target");
                    break;
			    case 1:	/* abs not abs_form */
			    case 3:	/* save is not a dir */
                    printerr("STOP. Target is not a directory");
                    break;
                default:
                    break;
			}
			rrr(); return;
		}
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) {
				strcpy(filename, Cellnow->fname);
				remove_tail(filename);
				printf("%s --> %s\n", Cellnow->fname, target);
				fflush(stdout);
				sprintf(cp_cmd, "cd \"%s\"; cp \"%s\" \"%s\""
					, CURRENT_DIR, filename, target);
				system(cp_cmd);
			}
		}
	} else if ( file_num == 1 ) {
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) break;
		}
		strcpy(filename, Cellnow->fname); remove_tail(filename);
		printf("%s --> %s\n", Cellnow->fname, target); fflush(stdout);
		sprintf(cp_cmd, "cd \"%s\"; cp \"%s\" \"%s\""
			, CURRENT_DIR, filename, target);
		system(cp_cmd);
	} else {
		strcpy(filename, POSITION->fname); remove_tail(filename);
		printf("%s --> %s\n", POSITION->fname, target); fflush(stdout);
		sprintf(cp_cmd, "cd \"%s\"; cp \"%s\" \"%s\""
			, CURRENT_DIR, filename, target);
		system(cp_cmd);
	}
	printf("### copy files is Completed. Press return : "); fflush(stdout);
	getchar();
	cbreak(); nonl(); noecho();
	re_read(CURRENT_DIR, 0);
	redraw();
}

void move_files()
{
	int file_num = 0;
	char target[DIRLEN];
	char mv_cmd[DIRLEN];
	char filename[FILELEN];

	endwin();
	file_num = check_selected_files();
	printf("\n\n##### MOVE FILES\n");
	if ( file_num ) {
		printf("### [ %d ] files selected.\n", file_num);
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) printf("%s  ", Cellnow->fname);
		}
		printf("\n");
	} else {
		printf("### [ %s ] is selected.\n", POSITION->fname);
	}
	printf("### Enter [target_dir] or [target_file]\n");
	printf("TO : "); fflush(stdout);
	fgets(target, DIRLEN, stdin); rtrim(target);
	if ( file_num > 1 ) {
		int ret;
		char temp_save[DIRLEN];
		if ( (ret = adjust_dir(CURRENT_DIR, target, temp_save)) ) {
			cbreak(); nonl(); noecho();
			redraw();
			switch( ret ) {
			    case 2:	/* can't open save */
                    printerr("STOP. Cannot open Target");
                    break;
			    case 1:	/* abs not abs_form */
			    case 3:	/* save is not a dir */
                    printerr("STOP. Target is not a directory");
                    break;
                default:
                    break;
			}
			rrr(); return;
		}
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) {
				strcpy(filename, Cellnow->fname);
				remove_tail(filename);
				printf("%s ==> %s\n", Cellnow->fname, target);
				fflush(stdout);
				sprintf(mv_cmd, "cd \"%s\"; mv \"%s\" \"%s\""
					, CURRENT_DIR, filename, target);
				system(mv_cmd);
			}
		}
	} else if ( file_num == 1 ) {
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) break;
		}
		strcpy(filename, Cellnow->fname); remove_tail(filename);
		printf("%s ==> %s\n", Cellnow->fname, target); fflush(stdout);
		sprintf(mv_cmd, "cd \"%s\"; mv \"%s\" \"%s\""
			, CURRENT_DIR, filename, target);
		system(mv_cmd);
	} else {
		strcpy(filename, POSITION->fname); remove_tail(filename);
		printf("%s ==> %s\n", POSITION->fname, target); fflush(stdout);
		sprintf(mv_cmd, "cd \"%s\"; mv \"%s\" \"%s\""
			, CURRENT_DIR, filename, target);
		system(mv_cmd);
	}
	printf("### move files is Completed. Press return : "); fflush(stdout);
	getchar();
	cbreak(); nonl(); noecho();
	re_read(CURRENT_DIR, 0);
	redraw();
}
int check_selected_files()
{
	int snum = 0;
	for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) 
		if ( SELECTED == 'y' ) snum++;
	return(snum);
}

/********************************************
    copy directory with sub_directories
********************************************/
void cp_dir()
{
	int file_num = 0;
	char target[DIRLEN];
	char cpdir_cmd[DIRLEN];
	char filename[FILELEN];

	switch ( SELECT_PARCEL ) {
	    case 's':
		printerr(""); rrr(); return;
	    case 'u':
		Parcelnow = DLPARCEL; strcpy(target, CURRENT_DIR);
		Parcelnow = DUPARCEL; break;
	    case 'l':
		Parcelnow = DUPARCEL; strcpy(target, CURRENT_DIR);
		Parcelnow = DLPARCEL; break;
	}
	endwin();
	file_num = check_selected_files();
	printf("\n\n##### COPY DIRECTORY with SUB-DIRECTORIES\n");
	printf("### Source dir = %s\n", CURRENT_DIR);
	if ( file_num ) {
		printf("### [ %d ] files or directories selected.\n", file_num);
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) printf("%s  ", Cellnow->fname);
		}
		printf("\n");
	}
	printf("### Target dir = %s\n", target);
	printf("### Continue (y/n) ? : "); fflush(stdout);
	if ( getchar() != 'y' ) {
		cbreak(); nonl(); noecho();
		redraw();
		printerr("cpdir STOP."); rrr(); return;
	}
	while ( getchar() != '\n' ) ;
	if ( file_num ) {
		for ( Cellnow = HEAD; Cellnow <= TAIL; Cellnow++ ) {
			if ( SELECTED == 'y' ) {
				strcpy(filename, Cellnow->fname);
				remove_tail(filename);
				sprintf(cpdir_cmd
				, "cd \"%s\"; find \"%s\" -depth -print |cpio -pdumv \"%s\""
				, CURRENT_DIR, filename, target);
				system(cpdir_cmd);
			}
		}
	} else {
		sprintf(cpdir_cmd
		, "cd \"%s\"; find . -depth -print | cpio -pdumv \"%s\""
		, CURRENT_DIR, target);
		system(cpdir_cmd);
	}
	printf("### cp_dir is Completed. Press return : "); fflush(stdout);
	getchar();
	cbreak(); nonl(); noecho();
	switch ( SELECT_PARCEL ) {
	    case 'u':
		Parcelnow = DLPARCEL; re_read(CURRENT_DIR, 0);
		Parcelnow = DUPARCEL; re_read(CURRENT_DIR, 0); break;
	    case 'l':
		Parcelnow = DUPARCEL; re_read(CURRENT_DIR, 0);
		Parcelnow = DLPARCEL; re_read(CURRENT_DIR, 0); break;
	}
	redraw();
}
/****************************************************************************
**	get_hostname	#get_hostname
****************************************************************************/
#ifdef USAGE
			switch( get_hostname() ) {
			    case 1:	/* uname failed */
			    case 2:	/* cuserid failed */
			    case 0:	/* success */
			}
#endif /* USAGE */

int get_hostname()
{
    struct utsname hhh;
    char uuu[FILELEN];
    char *go;

    uname(&hhh);
    if (!strcmp(hhh.nodename, ""))
    {
        return (1);
    }
    strcpy(Hostname, hhh.nodename);
    go = Hostname;
    while (*go != '\0')
    {
        if (*go <= 'z' && *go >= 'a')
        { *go += ('A' - 'a'); }
        go++;
    }

    if (cuserid(uuu) == NULL)
    {
        return (2);
    }
    strcpy(Username, uuu);
    go = Username;
    while (*go != '\0')
    {
        if (*go <= 'Z' && *go >= 'A')
        { *go += ('a' - 'A'); }
        go++;
    }

    return (0);
}

/*****************************
    compare directories
*****************************/
void comp_dir()
{
	char deg, msg[80];
	int file_num = 0;
	CELL *aaa = NULL, *bbb = NULL, *aaa_head = NULL, *bbb_head = NULL, *aaa_tail = NULL, *bbb_tail = NULL;
	switch ( SELECT_PARCEL ) {
	    case 's':
            printerr(""); rrr(); return;
	    case 'u':
            aaa_head = DUPARCEL->head + 2;
            aaa_tail = DUPARCEL->tail;
            bbb_head = DLPARCEL->head + 2;
            bbb_tail = DLPARCEL->tail;
            break;
	    case 'l':
            aaa_head = DLPARCEL->head + 2;
            aaa_tail = DLPARCEL->tail;
            bbb_head = DUPARCEL->head + 2;
            bbb_tail = DUPARCEL->tail;
            break;
	}
	printerr("Enter \"compare level\" : (n:name s:name&size m:name&size&mtime)");
	rrr();
	switch( getchar() ) {
	    case 's': deg = 's'; break;
	    case 'm': deg = 'm'; break;
	    default : deg = 'n';
	}
	for ( aaa = aaa_head; aaa <= aaa_tail; aaa++ ) {
		aaa->selected = 'n';
		for ( bbb = bbb_head; bbb <= bbb_tail; bbb++ ) {
			if ( comp_cell(aaa, bbb, deg) == 0 ) break;
		}
		if ( bbb > bbb_tail ) {
			aaa->selected = 'y';
			file_num++;
		}
	}
	redraw();
	sprintf(msg, "comp_dir [ %c ] : %d files selected", deg, file_num);
	printerr(msg); rrr();
}

int comp_cell(CELL* aaa, CELL* bbb, char deg)
	/* deg: n:name s:name&size m:name&size&modifytime */
{
	int comp_result = 0;
	if ( only_comp_name(aaa->fname, bbb->fname) != 0 ) comp_result = 1;
	if ( deg == 'n' ) return(comp_result);
	if ( aaa->list.st_size != bbb->list.st_size ) comp_result = 1;
	if ( deg == 's' ) return(comp_result);
	if ( aaa->list.st_mtime > bbb->list.st_mtime ) comp_result = 1;
		/* last one is important */
	return(comp_result);
}

int only_comp_name(char* ccc, char* ddd)
{
	char xxx[FILELEN], yyy[FILELEN];
	strcpy(xxx, ccc); strcpy(yyy, ddd);
	remove_tail(xxx); remove_tail(yyy);
	return( strcmp(xxx, yyy) );
}

/*****************************
 *      add on 4.02
 ****************************/

void getpass()
{
    struct passwd *pw;
    int nSize;
    ud = NULL;
    nud = 0;
    maxud = 0;
    setpwent();
    while ((pw = getpwent()) != NULL)
    {
        while (nud >= maxud)
        {
            maxud += UDQ;
            nSize = (int) sizeof(struct udata) * maxud;
            if (ud)
            {
                ud = (void *) realloc(ud, nSize);
            }
            else
            {
                ud = (void *) malloc(nSize);
            }
            if (ud == NULL)
            {
                fprintf(stderr, "abc: not enough memory for %d users\n", maxud);
                exit(1);
            }
        }

        /* copy fields from pw file structure to udata */
        ud[nud].uid = pw->pw_uid;
        strncpy(ud[nud].name, pw->pw_name, MAXLOGIN - 1);
        nud++;
    }
    endpwent();
}

/**	@brief	Get user name from uid
 */
char* name_of_user(uid_t nUid)
{
	static char szUsername[16];
	uid_t ii;

	/* search username database */
	for ( ii=0; ii<nud; ii++ )
		if ( ud[ii].uid == nUid )
			return ud[ii].name;

	/* not found, make string from nUid */
	sprintf(szUsername, "%d", nUid);

	return szUsername;
}

/**	@brief	Main function
 */
int main(int argc, char* argv[])
{
	/**********************/
	/* entering condition */
	/**********************/
	/* 1 : get startup dir */
	printf("Read Disk Information ... Wait ... "); fflush(stdout);
	if ( getcwd(Startupdir, DIRLEN) == (char *)NULL ) {
		printf("Cannot get current_directory. Stop.\n"); fflush(stdout);
		exit(1);
	}
    if (argc > 1)
    {
		char relative[DIRLEN];
		char save[DIRLEN];
		strcpy(relative, *(argv+1));
        switch (adjust_dir(Startupdir, relative, save))
        {
            case 1:    /* abs not abs_form */
                printf("Sorry. Startup directory is not correct.\n"); fflush(stdout);
                exit(1);
            case 2:    /* can't open save */
                printf("Cannot open directory %s\n", save); fflush(stdout);
                exit(2);
            case 3:    /* save is not a dir */
                printf("%s is not a directory.\n", save); fflush(stdout);
                exit(3);
            case 0:    /* success */
                strcpy(Startupdir, save);
                break;
        }
	}
	if (Startupdir[0])
    {
        char tmp_str[DIRLEN];
        switch (get_hostname())
        {
            case 1:    /* uname failed */
            case 2:    /* cuserid failed */
                printf("Get hostname failed. \nEnter hostname : "); fflush(stdout);
                fgets(tmp_str, DIRLEN, stdin); rtrim(tmp_str);
                tmp_str[30] = '\0';
                strcpy(Hostname, tmp_str);
                printf("Enter username : "); fflush(stdout);
                fgets(tmp_str, DIRLEN, stdin); rtrim(tmp_str);
                tmp_str[30] = '\0';
                strcpy(Username, tmp_str);
                break;
            case 0:    /* success */
                break;
        }
    }
	getpass();

	/* 2 : init screen */
    printf("\nInit screen ... Wait ... "); fflush(stdout);
	initscr(); cbreak(); noecho(); nonl();
    printf("COLS=%d, LINES=%d\n", COLS, LINES); fflush(stdout);
	move(LINES/2-4, (COLS-17)/2-1);		/* logo */
	printw("$$$$$$$$$$$$$$$$$");
	move(LINES/2-3, (COLS-17)/2-1);
	printw("$$$$   ABC   $$$$");
	move(LINES/2-2, (COLS-17)/2-1);
	printw("$$$$$$$$$$$$ %s", ABC_VERSION);
	move(LINES/2, (COLS-26)/2-1);
	printw("Easily control directories");
	move(LINES/2+1, (COLS-12)/2-1);
	printw("-- Yu Wan --");
	printerr("Press any key");
	rrr(); getchar();		/* end of logo */
	Sep_line = (LINES - 1) / 2;
	/* 3 : start mud */
	{ int i; for( i=0; i<MUD_MAX_CNT; i++ ) Muds[i] = (MUD *)NULL; }
	Mud_num = 0;
	start_mud("Start Up MUD");
	printerr("help : ? "); rrr();

	/***************************/
	/* toss to command station */
	/***************************/
	com_station();

	/*********************/
	/* exiting condition */
	/*********************/
	/* 1 : free memory segments */
	/*     exit() will do this for me */
	/* 2 : end screen */
	clear(); rrr(); endwin();
}
