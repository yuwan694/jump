#!/bin/bash
# set -x

echo "> Warning: This script is NOT working well with filenames with spaces!"

for avi in `ls -1 *.avi *.wmv *.mpeg *.mpg *.mkv | sort`
do
	mp4="${avi%.*}.mp4"
	if [ ! -f "${mp4}" ];
	then
		echo "> Processing \"${avi}\", Press Enter to start ..."
		read a
		/usr/bin/cpulimit -f -l 200 -- \
			/usr/bin/ffmpeg \
				-n \
				-i ${avi} \
				${mp4}
	fi
done
