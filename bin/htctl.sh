#!/bin/bash
## This script helps to turn on/off hyper-threading.

warning()
{
	echo "> Please be informed that there's better way to turn on/off hyper-threading:"
	echo "> 1) Use 'nosmt' or 'nosmt=force' kernel command line parameter"
	echo "> 2) See /sys/devices/system/cpu/smt/{active,control}"
	echo "     echo on/off/forceoff > /sys/devices/system/cpu/smt/control"
	echo "> Just look at this script as an alternative way."
	exit 5
}

REQUEST=
HYPER_THREADS=

check_super_user()
{
	if [ "$EUID" != "0" ]
	then
		echo "> Hyper-threading control requires super user priviledge."
		echo "  Please run this script again, as root user."
		exit 1
	fi
}

get_arguments()
{
	if [ $# -lt 1 ]
	then
		echo "> Usage: $0 0/1"
		echo "  where 0 to turn OFF secondary hyper-threads,"
		echo "        1 to trun ON all hyper-threads"
		exit 2
	fi

	case "$1" in
	0|1)
		REQUEST=$1;;
	*)
		echo "> Invalid request: $1";
		exit 4;;
	esac

	echo "> Request: $REQUEST"
}

get_hyper_threads()
{
	if [ "$REQUEST" == "0" ]
	then
		HYPER_THREADS=$(grep -ohE ',[0-9]+' /sys/devices/system/cpu/cpu*/topology/thread_siblings_list \
						| grep -oE '[0-9]+' \
						| sort -n \
						| uniq \
						| paste -sd ' ')
	else
		HYPER_THREADS=$(grep -Ho 0 /sys/devices/system/cpu/cpu*/online \
						| grep -oE 'cpu[0-9]+' \
						| grep -oE '[0-9]+' \
						| sort -n \
						| uniq \
						| paste -sd ' ')
	fi

	if [ "$HYPER_THREADS" == "" ]
	then
		echo "> No hyper-threads for request=$REQUEST."
		exit 3
	fi

	echo "> Hyper-threads for request=$REQUEST: $HYPER_THREADS"
}

control_hyper_threads()
{
	echo "> Commit the request=$REQUEST:"
	for HT in $HYPER_THREADS
	do
		echo "  ...writing $REQUEST to cpu${HT}/online..."
		echo $REQUEST > /sys/devices/system/cpu/cpu${HT}/online;
	done
}

cpus_info()
{
	PRESENT_CPUS=$(cat /sys/devices/system/cpu/present)
	ISOLATED_CPUS=$(cat /sys/devices/system/cpu/isolated)
	ONLINE_CPUS=$(cat /sys/devices/system/cpu/online)
	OFFLINE_CPUS=$(cat /sys/devices/system/cpu/offline)
	echo "> CPUs detected:"
	echo "   present: $PRESENT_CPUS"
	echo "  isolated: $ISOLATED_CPUS"
	echo "    online: $ONLINE_CPUS"
	echo "   offline: $OFFLINE_CPUS"
}

warning
cpus_info
get_arguments "$@"
get_hyper_threads
check_super_user
control_hyper_threads
cpus_info
