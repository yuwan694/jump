source $HOME/bin/j.sh
alias did="vi +'normal Go' +'r!date' ~/.did.txt"
alias todo="vim ~/.todo.txt"
export CC=gcc-7
export CXX=g++-7
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/
export PATH=$PATH:$JAVA_HOME/bin
