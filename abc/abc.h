/**	@file	abc.h
 *	@brief	ABC main header
 *
 *	@author	yuwan@innopiatech.com
 *	@date	2005. 10. 18.
 */

#ifndef _ABC_H
#define _ABC_H

#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <time.h>
#include <pwd.h>
#include <sys/utsname.h>
#include <ctype.h>

/** global definitions */
#define ABC_VERSION	"5.01"		/* abc for Linux */
#define ABCRC		".abcrc"	/* abc config (resource) file */
#define	DIRLEN		1024
#define FILELEN 	256
#define MUD_MAX_CNT	19
#define TITLELEN	64

/** key-value define */
#define CRTL_F	'\06'
#define CRTL_B	'\02'
#define CRTL_E	'\05'
#define CRTL_Y	'\031'
#define CRTL_G	'\07'
#define CRTL_L	'\014'

#define FNAME_TAILES	"?*|/@=>%"	//!< filename tailes.
#define FORCE_MOVE	1		//!< force move over the edge

#define CURRENT_FILE_TAG    " <<"


/********************************/
/* 	CELL #cell		*/
/********************************/
#ifdef USAGE
switch( SELECTED ) {
		    case 'y':	/* this cell is selected */
		    case 'n':	/* not selected */
		}
#endif
typedef struct cell {
    char		fname[FILELEN];
    char		selected;
#define	SELECTED	(Cellnow->selected)
    struct stat	list;	/* status of this file */
} CELL;

/********************************/
/*	PARCEL #parcel		*/
/********************************/
#ifdef USAGE
switch( PARCEL_MODE ) {
		    case 's':	/* single - normal */
		    case 'S':	/* single - long */
		    case 'u':	/* double - up - normal */
		    case 'U':	/* double - up - long */
		    case 'l':	/* double - low - normal */
		    case 'L':	/* double - low - long */
		}
		switch( UPDATE ) {
		    case 'y':	/* updated */
		    case 'n':	/* not updated */
		}
		switch( SORT_METHOD ) {
		    case 'n':	/* name */
		    case 't':	/* time */
		    case 'e':	/* extention */
		    case 'd':	/* directory & executable */
		}
#endif
typedef struct parcel {
    char	current_dir[DIRLEN];
#define CURRENT_DIR	(Parcelnow->current_dir)
    char	blank_cell[DIRLEN];
#define BLANK_CELL	(Parcelnow->blank_cell)
    int	cell_per_col;
    int	col_width;
    int	num_of_cell;
#define	CELL_PER_COL	(Parcelnow->cell_per_col)
#define COL_WIDTH	(Parcelnow->col_width)
#define NUM_OF_CELL	(Parcelnow->num_of_cell)
    char	parcel_mode;
#define	PARCEL_MODE	(Parcelnow->parcel_mode)
    char	update;
#define	UPDATE		(Parcelnow->update)
    char	sort_method;
#define	SORT_METHOD	(Parcelnow->sort_method)
    CELL	*head;
    CELL	*tail;
    CELL	*dsphead;
    CELL	*position;
#define	HEAD		(Parcelnow->head)
#define	TAIL		(Parcelnow->tail)
#define	DSPHEAD		(Parcelnow->dsphead)
#define	POSITION	(Parcelnow->position)
    int	dir_line;
    int	mini_line;
    int	upmost_line;
    int	downlast_line;
#define DIR_LINE	(Parcelnow->dir_line)
#define MINI_LINE	(Parcelnow->mini_line)
#define UPMOST_LINE	(Parcelnow->upmost_line)
#define DOWNLAST_LINE	(Parcelnow->downlast_line)
} PARCEL;

/********************************/
/*	MUD #mud		*/
/********************************/
#ifdef USAGE
switch( SELECT_PARCEL ) {
		    case 's':	/* single is selected */
		    case 'u':	/* double up */
		    case 'l':	/* double down */
		}
#endif
typedef struct mud {
    char	title[TITLELEN];
#define TITLE		(Mudnow->title)
    char	search_pattern[FILELEN];
#define SEARCH_PATTERN	(Mudnow->search_pattern)
    char	select_parcel;
#define	SELECT_PARCEL	(Mudnow->select_parcel)
    PARCEL	*sparcel;
    PARCEL	*duparcel;
    PARCEL	*dlparcel;
#define	SPARCEL		(Mudnow->sparcel)
#define	DUPARCEL	(Mudnow->duparcel)
#define	DLPARCEL	(Mudnow->dlparcel)
} MUD;


char *trimwhitespace(char *str);
char *ltrim(char *str);
char *rtrim(char *str);
void save_cur_dir(void);
int adjust_dir(char* abs, char* rel, char* save);
void com_station();
void mud_change();
void mud_mode_change();
void parcel_line();
int get_newparcel(char* pathname, char sort_meth);
void printerr(char* msg);
void printerr2(char* msg);
int rrr(void);
void get_file_status(char* name);
void qsort_cell(CELL* left_edge, CELL* right_edge);
void parcel_mode_change();
void toggle_dmode();
void move_down();
void move_down_page();
void move_up();
void move_up_page();
void draw_parcel();
void draw_dir();
int draw_cell(int insist);
void draw_mini(int mline);
void start_mud(char* mud_name);
int get_newmud();
void get_str(char* temp, char* msg, int len);
void search_fname(int insist);
int search(char* AAA);
int search_cmp(char* AAA, char* BBB);
void shell_cmd();
void help_me();
void edit_file(char flag);
void delete_files(char flag);
int check_selected_files();
void cp_dir();
int get_hostname();
void comp_dir();
int comp_cell(CELL* aaa, CELL* bbb, char deg);
int only_comp_name(char* ccc, char* ddd);
void getpass();
char* name_of_user(uid_t nUid);
void move_files();
void copy_files();
void over_comp(char flag);
void over_send(char flag);
void sort_method();
void swap_cell(CELL* AAA, CELL* BBB);
int str_cmp(CELL* AAA, CELL* BBB);
void re_read(char* pathname, int redraw_flag);
void remove_tail(char* filename);
void cd_argu();
void change_dir();
void redraw();
void file_num();
void select_cell();
void select_all();
void unselect();
void unselect_all();
void tell_select();
void move_position(int num);
void move_head(int flag);
int move_up_line(void);
int move_down_line(void);
int move_right(int bForce);
int move_left(void);


/** Proto-types */
#ifndef _ABC_C
extern char* sample_string(char* pOrig, int nMax);
extern void name_mud(void);
#else /* _ABC_C */
char* sample_string(char* pOrig, int nMax);
void name_mud(void);
#endif /* _ABC_C */

#endif /* _ABC_H */
